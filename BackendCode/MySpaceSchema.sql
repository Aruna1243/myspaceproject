
SELECT * FROM myspace_db.login_credentials;

SELECT * FROM myspace_db.employeesalary_details;

SELECT * FROM myspace_db.employee_details;

SELECT * FROM myspace_db.training_sections_tb;

SELECT * FROM myspace_db.employeesalary_details;

SELECT * FROM myspace_db.leave_details;

SELECT * FROM myspace_db.training_sections_tb;

drop table myspace_db.training_sections_tb;

UPDATE `myspace_db`.`training_sections_tb` SET `start_date` = '2019-02-12', `end_date` = '2019-05-12' WHERE (`domain_id` = '1');
UPDATE `myspace_db`.`training_sections_tb` SET `start_date` = '2019-02-12', `end_date` = '2019-05-12' WHERE (`domain_id` = '2');
UPDATE `myspace_db`.`training_sections_tb` SET `start_date` = '2019-02-12', `end_date` = '2019-05-12' WHERE (`domain_id` = '3');
UPDATE `myspace_db`.`training_sections_tb` SET `start_date` = '2019-03-06', `end_date` = '2019-04-06' WHERE (`domain_id` = '4');
UPDATE `myspace_db`.`training_sections_tb` SET `start_date` = '2019-03-06', `end_date` = '2019-04-06' WHERE (`domain_id` = '5');
UPDATE `myspace_db`.`training_sections_tb` SET `start_date` = '2019-04-05', `end_date` = '2019-06-05' WHERE (`domain_id` = '6');
UPDATE `myspace_db`.`training_sections_tb` SET `start_date` = '2019-03-06', `end_date` = '2019-06-06' WHERE (`domain_id` = '7');
INSERT INTO `myspace_db`.`training_sections_tb` (`domain_id`, `title`, `start_date`, `end_date`) VALUES ('8', 'erewg', '2019-03-06', '2019-08-06');


CREATE TABLE `myspace_db`.`training_sessions_tbl` (
  `domain_id` INT NOT NULL,
  `title` VARCHAR(45) NULL,
  `start_date` DATE NULL,
  `end_date` DATE NULL,
  PRIMARY KEY (`domain_id`));


SELECT * FROM myspace_db.training_sessions_tbl;

INSERT INTO `myspace_db`.`training_sessions_tbl` (`domain_id`, `title`, `start_date`, `end_date`) VALUES ('1', 'Salesforce', '2019-02-12', '2019-06-12');
INSERT INTO `myspace_db`.`training_sessions_tbl` (`domain_id`, `title`, `start_date`, `end_date`) VALUES ('2', 'Java', '2019-02-12', '2019-05-12');
INSERT INTO `myspace_db`.`training_sessions_tbl` (`domain_id`, `title`, `start_date`, `end_date`) VALUES ('3', 'UI', '2019-03-23', '2019-06-16');
INSERT INTO `myspace_db`.`training_sessions_tbl` (`domain_id`, `title`, `start_date`, `end_date`) VALUES ('4', 'RPA', '2019-06-15', '2019-08-25');
INSERT INTO `myspace_db`.`training_sessions_tbl` (`domain_id`, `title`, `start_date`, `end_date`) VALUES ('5', 'Embedded', '2019-05-24', '2019-08-31');
INSERT INTO `myspace_db`.`training_sessions_tbl` (`domain_id`, `title`, `start_date`, `end_date`) VALUES ('6', 'DataScience', '2019-04-07', '2019-07-20');
INSERT INTO `myspace_db`.`training_sessions_tbl` (`domain_id`, `title`, `start_date`, `end_date`) VALUES ('7', 'BigData', '2019-03-10', '2019-07-17');

CREATE TABLE `myspace_db`.`cultural_events` (
  `event_id` INT,
  `event_name` VARCHAR(45) NULL,
  `description` VARCHAR(500) NULL,
  `start_date` DATE NULL,
  `end_date` DATE NULL,
  PRIMARY KEY (`event_id`));

drop table myspace_db.cultural_events;

SELECT * FROM myspace_db.cultural_events;

INSERT INTO `myspace_db`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('1', 'InnomindsBash', 'Innominds friday night party including Feast.', '2019-02-19', '2019-03-24');
INSERT INTO `myspace_db`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('2', 'HostWorkshop', 'Innominds, in collaboration with the ‘Interaction Design Foundation,’ organized a design workshop on the topic, “Challenges of Designers in Corporates”. ', '2019-02-19', '2019-03-24');
INSERT INTO `myspace_db`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('3', 'FlashMob', 'Innominds is conducting a flashmob for all the employee\'s who are intrested.', '2019-02-28', '2019-02-28');
INSERT INTO `myspace_db`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('4', 'BasketBallTournament', 'Innominds is conducting a tournament for the bsketball players', '2019-03--03', '2019-03-03');
INSERT INTO `myspace_db`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('5', 'TeamOuting', 'Innominds is conducting an outing for employees for every 3 months', '2019-04-04', '2019-07-05');
INSERT INTO `myspace_db`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('6', 'TableTennis', 'TableTennis Tournament every friday', '2019-01-02', '2020-01-01');
INSERT INTO `myspace_db`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('7', 'gjhmk', 'fgk', '2019-01-02', '2020-01-01');
INSERT INTO `myspace_db`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('8', 'xdfh', 'xdfh', '2019-01-02', '2020-01-01');
INSERT INTO `myspace_db`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('9', 'bkj', 'kvjk', '2019-01-02', '2020-01-01');
INSERT INTO `myspace_db`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('10', 'sdfg', 'adsg', '2019-01-02', '2020-01-01');