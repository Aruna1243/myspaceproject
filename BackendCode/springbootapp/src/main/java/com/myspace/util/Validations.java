package com.myspace.util;

import java.util.regex.Pattern;

public class Validations {

	
	public static boolean validateEmailId(String companyEmailId)
	{
		boolean status = false;
		String email = "^[a-zA-Z0-9_+*&-]+(?:\\."+"[a-zA-Z0-9_+*&-]+)*@"+"(?:[a-zA-Z0-9-]+\\.)+[a-z"+"A-Z]{2,7}$";
		Pattern pat = Pattern.compile(email);
		if(pat.matcher(companyEmailId).matches())
		{
			
			status = true;
		
		}
		
		return status;
	}
}
