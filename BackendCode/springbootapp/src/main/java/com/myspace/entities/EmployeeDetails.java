package com.myspace.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This is the entity class in which we can create database tables and generate
 * setters and getters
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "employee_details")
public class EmployeeDetails {

	// @Id represents the primary key attribute
	@Id
	@Column(name = "employee_id")
	private int employeeId;
	// it represents the database column
	@Column(name = "employee_name")
	private String name;

	@Column(name = "employment_type")
	private String employmentType;

	@Column(name = "department")
	private String department;

	@Column(name = "designation")
	private String designation;

	@Column(name = "practice")
	private String practice;

	@Column
	private String competency;

	@Column(name = "work_location")
	private String workLocation;

	@Column(name = "permanent_location")
	private String permanentLocation;

	@Column(name = "company_email_id")
	private String companyEmailId;

	@Column(name = "personal_email_id")
	private String personalEmailId;

	@Column(name = "work_place_phone_number")
	private String workPlacePhoneNumber;

	@Column(name = "primary_mobile_number")
	private String primaryMobileNumber;

	@Column(name = "alternative_mobile_number")
	private String alternativeMobileNumber;

	@Column(name = "skpe_id")
	private String skypeId;

	@Column(name = "job_status")
	private String jobStatus;

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getPractice() {
		return practice;
	}

	public void setPractice(String practice) {
		this.practice = practice;
	}

	public String getCompetency() {
		return competency;
	}

	public void setCompetency(String competency) {
		this.competency = competency;
	}

	public String getWorkLocation() {
		return workLocation;
	}

	public void setWorkLocation(String workLocation) {
		this.workLocation = workLocation;
	}

	public String getPermanentLocation() {
		return permanentLocation;
	}

	public void setPermanentLocation(String permanentLocation) {
		this.permanentLocation = permanentLocation;
	}

	public String getCompanyEmailId() {
		return companyEmailId;
	}

	public void setCompanyEmailId(String companyEmailId) {
		this.companyEmailId = companyEmailId;
	}

	public String getPersonalEmailId() {
		return personalEmailId;
	}

	public void setPersonalEmailId(String personalEmailId) {
		this.personalEmailId = personalEmailId;
	}

	public String getWorkPlacePhoneNumber() {
		return workPlacePhoneNumber;
	}

	public void setWorkPlacePhoneNumber(String workPlacePhoneNumber) {
		this.workPlacePhoneNumber = workPlacePhoneNumber;
	}

	public String getPrimaryMobileNumber() {
		return primaryMobileNumber;
	}

	public void setPrimaryMobileNumber(String primaryMobileNumber) {
		this.primaryMobileNumber = primaryMobileNumber;
	}

	public String getAlternativeMobileNumber() {
		return alternativeMobileNumber;
	}

	public void setAlternativeMobileNumber(String alternativeMobileNumber) {
		this.alternativeMobileNumber = alternativeMobileNumber;
	}

	public String getSkypeId() {
		return skypeId;
	}

	public void setSkypeId(String skypeId) {
		this.skypeId = skypeId;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

}
