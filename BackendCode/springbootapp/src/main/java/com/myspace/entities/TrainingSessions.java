package com.myspace.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This is the entity class in which we can create database tables and generate
 * setters and getters
 * 
 * @author IMVIZAG
 *
 */

@Entity
//it represents the database table
@Table(name = "TrainingSections_tb")
public class TrainingSessions {

	// @Id represents the primary key attribute
    
	// it represents the database column
	@Id 
	@Column(name = "domain_id")
	private int domainId;
	@Column
	private String title;
	@Column(name = "start_date")
	private String start;
	@Column(name = "end_date")
	private String end;

	// setters and getters
	public int getDomainId() {
		return domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	
}
