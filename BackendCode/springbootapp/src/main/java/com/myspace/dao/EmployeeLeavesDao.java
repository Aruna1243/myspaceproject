package com.myspace.dao;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import com.myspace.entities.EmployeeLeaves;


/**
 * This is the dao class which extends the crud repository
 * 
 * @author IMVIZAG
 *
 */

   @Repository
	public interface EmployeeLeavesDao  extends CrudRepository<EmployeeLeaves, Integer>{


}