package com.myspace.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myspace.entities.EmployeeDetails;
import com.myspace.entities.EmployeeLeaves;
import com.myspace.entities.EmployeeSalary;
import com.myspace.entities.LoginCredentials;
import com.myspace.service.EmployeeServiceImpl;
import com.myspace.util.JwtImpl;

/**
 * 
 * @author IMVIZAG
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/myspace")
public class EmployeeControllerImpl {

	@Autowired
	private EmployeeServiceImpl employeeServiceImpl;

	/**
	 * this method is used for the employee to login into the application based on
	 * username password
	 * 
	 * @param employee
	 * @return
	 */

	@PostMapping("/login")
	public ResponseEntity<?> doAuthenticate(@RequestBody LoginCredentials credentials) {
		// calling service method
		int empId = employeeServiceImpl.doLogin(credentials.getUsername(), credentials.getPassword());

		if (empId == -1) {
			// if username wrong it returns 202 status code along with message
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
					.body("{\"status\":\"username does not exist\",\"statuscode\":" + 202 + "}");
		} else if (empId == -2) {
			// if password is wrong it returns 201 status code along with message
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
					.body("{\"status\":\"wrong password\",\"statuscode\":" + 201 + "}");
		}
		String id = "" + empId;

		// if valid user generating token for that user
		String token = JwtImpl.createJWT(id);
		// if authentication is success it will return empId along with 200 status code
		// and success message.
		ObjectMapper map = new ObjectMapper();
		String jToken = null;
		try {
			jToken = map.writeValueAsString(token);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"login success\",\"statuscode\":" + 200
				+ ",\"empId\":" + empId + ",\"token\":" + jToken + "}");
	} // End of doAuthenticate

	/**
	 * this method is used for the employee to view his profile based on employee_id
	 * 
	 * @param employee
	 * @return
	 */

	@GetMapping("/viewprofile/{employee_id}")
	public ResponseEntity<EmployeeDetails> getById(@PathVariable("employee_id") int id) {

		EmployeeDetails employee = employeeServiceImpl.findById(id);
		if (employee != null) {
			return new ResponseEntity<EmployeeDetails>(employee, HttpStatus.OK);
		}

		return new ResponseEntity<EmployeeDetails>(HttpStatus.NO_CONTENT);

	}

	/**
	 * this method is used for the employee to search his profile based on
	 * employee_id
	 * 
	 * @param employee_id
	 * @return
	 */
	@GetMapping("/searchEmployeeById/{employee_id}")
	public @ResponseBody ResponseEntity<String> getAll(@PathVariable("employee_id") int id) {
		List<EmployeeDetails> entityList = employeeServiceImpl.searchById(id);

		if (entityList != null) {
			// return new ResponseEntity<>(entityList, HttpStatus.OK);
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Found Employee\",\"statuscode\":" + 200 + "}");
		}
		// return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		return ResponseEntity.status(HttpStatus.NOT_FOUND

		).body("{\"status\":\"Not Found\",\"statuscode\":" + 201 + "}");
	}

	/**
	 * this method is used for the employee to search his profile based on
	 * employee_name
	 * 
	 * @param employee_id
	 * @return
	 */
	@GetMapping("/searchEmployeeByName/{name}")
	public @ResponseBody ResponseEntity<?> getByName(@PathVariable("name") String name) {
		EmployeeDetails optional = employeeServiceImpl.findUserByName(name);

		if (optional != null) {
//	return ResponseEntity.status(HttpStatus.OK)
//					.body("{\"status\":\"Found Employee\" ,\"statuscode\":" + 200 + "}");
			return new ResponseEntity<>(optional, HttpStatus.OK);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body("{\"status\":\"Not Found\",\"statuscode\":" + 201 + "}");
	}

	/**
	 * this method is used for the employee to search his profile based on
	 * companyEmailId
	 * 
	 * @param companyEmailId
	 * @return
	 */
	@GetMapping("/searchEmployeeByEmailId/{companyEmailId}")
	public @ResponseBody ResponseEntity<?> getByEmailId(@PathVariable("companyEmailId") String companyEmailId) {
		EmployeeDetails details = employeeServiceImpl.findUserByEmailId(companyEmailId);

		if (details != null) {
//		return ResponseEntity.status(HttpStatus.OK)
//					.body( "{\"status\":\"Found Employee\",\"statuscode\":" + 200 + "}");
			return new ResponseEntity<>(details, HttpStatus.OK);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body("{\"status\":\"Not Found\",\"statuscode\":" + 201 + "}");
	}

	/**
	 * this method is used for the employee to update his profile based on
	 * 
	 * @param employee_id
	 * @return
	 */
	@PutMapping("/updateprofile/{employee_id}")
	public ResponseEntity<?> updateProfileById(@PathVariable("employee_id") int id,
			@RequestBody EmployeeDetails employee) {

		EmployeeDetails emp = employeeServiceImpl.update(id, employee);
		if (emp == null) {
			// return new ResponseEntity<EmployeeDetails>(HttpStatus.NO_CONTENT);
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
					.body("{\"status\":\"not updated\",\"statuscode\":" + 201 + "}");
		}

		// return new ResponseEntity<EmployeeDetails>(emp, HttpStatus.OK);
		return ResponseEntity.status(HttpStatus.ACCEPTED).body("{\"status\":\"updated\",\"statuscode\":" + 200 + "}");
	}

	/**
	 * this method is used for the employee to apply for the leaves
	 * 
	 * @param employee_id
	 * @return
	 */
	@PostMapping("/leavedetails")
	public ResponseEntity<?> applyForLeave(@RequestBody EmployeeLeaves employee) {
		EmployeeLeaves leaves = employeeServiceImpl.applyForLeave(employee);

		if (leaves == null) {
			// return new ResponseEntity<EmployeeLeaves>(HttpStatus.NO_CONTENT);
			return ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body("{\"status\":\"not updated\",\"statuscode\":" + 201 + "}");

		}

		// return new ResponseEntity<EmployeeLeaves>(leaves, HttpStatus.OK);
		return ResponseEntity.status(HttpStatus.ACCEPTED)
				.body("{\"status\":\"not updated\",\"statuscode\":" + 200 + "}");
	}

	/**
	 * this method is used for the employee to view his profile based on employee_id
	 * 
	 * @param employee
	 * @return
	 */

	@PutMapping("/salarydetails")
	public ResponseEntity<EmployeeSalary> salaryDetails(@RequestBody EmployeeSalary salarydetails) {
		EmployeeSalary details = employeeServiceImpl.getSalaryDetails(salarydetails);

		if (details == null) {
			return new ResponseEntity<EmployeeSalary>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<EmployeeSalary>(details, HttpStatus.OK);
	}

	@PutMapping("/changepassword/{employeeId}/{password}/{new_password}/{confirm_new_password}")
	public ResponseEntity<?> changePassword(@PathVariable("employeeId") int id,
			@PathVariable("password") String password, @PathVariable("new_password") String new_password,
			@PathVariable("confirm_new_password") String confirm_new_password, @RequestHeader HttpHeaders headers) {

		String token = "";

		ObjectMapper mapper = new ObjectMapper();

		String jsonResponce = null;

		
		Map<String, Object> projectsMap = new HashMap<>();
		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

		token = hList.get(0);

		if (JwtImpl.isValidUser(id, token)) {

			// it will call searchPassword of the employeeService class
			LoginCredentials loginCredentials = employeeServiceImpl.searchPassword(password);
			System.out.println(loginCredentials);
			// if the object is not null then it will return LoginCredentials object
			 System.out.println();
			if (loginCredentials != null) {
				System.out.println("hello");
				// if password is existed

				projectsMap.put("status", "not entersed");
				// if both new password are matched then it will update
				if (new_password.equals(confirm_new_password)) {

					loginCredentials.setPassword(new_password);
					employeeServiceImpl.updatePassword(loginCredentials);
					projectsMap.put("status", 200);
					projectsMap.put("password_updated", loginCredentials.getPassword());

					try {
						jsonResponce = mapper.writeValueAsString(projectsMap);
					} catch (JsonProcessingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);

				}
				// if both new_password and confirm passwords are not matched
				else {

					projectsMap.put("status", "password you entered is not matched");
					projectsMap.put("statuscode", 204);
					try {
						jsonResponce = mapper.writeValueAsString(projectsMap);
					} catch (JsonProcessingException e) {
						e.printStackTrace();

					}
					return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
				}

			} else {

				// password is not existed

				projectsMap.put("status", "password is doesn't exist");
				projectsMap.put("statuscode", 204);
				try {
					jsonResponce = mapper.writeValueAsString(projectsMap);
				} catch (JsonProcessingException e) {
					e.printStackTrace();

				}
				return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);

			}
		}

		else {

			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	}
}