package com.myspace.service;

import java.util.List;

import com.myspace.entities.EmployeeDetails;
import com.myspace.entities.EmployeeLeaves;
import com.myspace.entities.EmployeeSalary;
import com.myspace.entities.LoginCredentials;

public interface EmployeeService {
	/**
	 * This abstract method gets employee from databse as per given id
	 * 
	 * @param emmployeeId
	 * @return Employee object
	 */
	int doLogin(String username, String password);

	EmployeeDetails findById(int employeeId);

	public EmployeeDetails update(int employeeId, EmployeeDetails employee);

	public List<EmployeeDetails> searchById(int id);

	public EmployeeLeaves applyForLeave(EmployeeLeaves employee);

	public EmployeeSalary getSalaryDetails(EmployeeSalary salarydetails);

//	public List<EmployeeDetails> findByEmail(String companyEmailId);

	public EmployeeDetails findUserByName(String name);

	public EmployeeDetails findUserByEmailId(String companyEmailId);

	public void updatePassword(LoginCredentials loginCredentials);

	public LoginCredentials searchPassword(String employee_password);

}
