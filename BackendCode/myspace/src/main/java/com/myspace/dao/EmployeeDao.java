package com.myspace.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.myspace.entities.EmployeeDetails;

/**
 * This is the dao class which extends the crud repository
 * 
 * @author IMVIZAG
 *
 */

@Repository
public interface EmployeeDao extends JpaRepository<EmployeeDetails, Integer> {

	Optional<EmployeeDetails> findByName(String name);

	Optional<EmployeeDetails> findBycompanyEmailId(String companyEmailId);

}
