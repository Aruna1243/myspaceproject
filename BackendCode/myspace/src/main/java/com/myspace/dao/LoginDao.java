package com.myspace.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.myspace.entities.LoginCredentials;

/**
 * This is the DAO class which extends the CRUD repository
 * 
 * @author IMVIZAG
 *
 */
public interface LoginDao extends CrudRepository<LoginCredentials, Integer> {
	public LoginCredentials findByUsername(String username);

	@Query(value = "select * from login_credentials where password = ?", nativeQuery = true)
	public LoginCredentials findBypassword(String password);
}