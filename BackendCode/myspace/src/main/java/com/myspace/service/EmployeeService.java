package com.myspace.service;

import java.util.List;

import com.myspace.entities.EmployeeDetails;
import com.myspace.entities.EmployeeLeaves;
import com.myspace.entities.LoginCredentials;

/**
 * This abstract method gets employee from database as per given id
 * 
 * @author IMVIZAG
 *
 */
public interface EmployeeService {

	int doLogin(String username, String password);

	EmployeeDetails findById(int employeeId);

	public EmployeeDetails update(int employeeId, EmployeeDetails employee);

	public List<EmployeeDetails> searchById(int id);

	public EmployeeLeaves getSalaryDetails(int id);

	public EmployeeLeaves applyForLeave(int id, EmployeeLeaves employee);

	public EmployeeDetails findUserByName(String name);

	public EmployeeDetails findUserByEmailId(String companyEmailId);

	public void updatePassword(LoginCredentials loginCredentials);

	public LoginCredentials searchPassword(String employee_password);

}
