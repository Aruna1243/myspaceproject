package com.myspace.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * This is the entity class in which we can create database tables and generate
 * setters and getters
 * 
 * @author IMVIZAG
 *
 */

@Entity
@Table(name = "employeesalary_details")
public class EmployeeSalary {
	// @Id represents the primary key attribute
    @Id 
	@Column(name = "employee_id")
	private int employeeId;
    
	@Column(name="leaves")
	private int numberOfLeaves;
	
	@Column(name="employee_salary")
	private int employeesalary;


	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getNumberOfLeaves() {
		return numberOfLeaves;
	}

	public void setNumberOfLeaves(int numberOfLeaves) {
		this.numberOfLeaves = numberOfLeaves;
	}

	public int getEmployeesalary() {
		return employeesalary;
	}

	public void setEmployeesalary(int employeesalary) {
		this.employeesalary = employeesalary;
	}

	
}
