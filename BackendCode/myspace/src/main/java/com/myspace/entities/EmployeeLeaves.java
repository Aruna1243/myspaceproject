package com.myspace.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This is the entity class in which we can create database tables and generate
 * setters and getters
 * 
 * @author IMVIZAG
 *
 */

@Entity
// it represents the database table
@Table
public class EmployeeLeaves {

	// @Id represents the primary key attribute
	@Id
	@Column(name = "employee_id")
	private int employeeId;

	@Column
	private String username;

	@Column
	private String leaveType;

	@Column
	private String reason;

	@Column
	private int totalNoOfLeaves;

	@Column(name = "from_date")
	private String fromdate;

	@Column(name = "to_date")
	private String todate;

	@Column
	private int totalsalary;

	@Column
	private int numberOfLeaves;

	@Column(name = "basic_pay")
	private int basicPay;

	@Column(name = "hra")
	private int hra;

	@Column(name = "statutory_bonus")
	private int statutoryBonus;

	@Column(name = "provident_fund")
	private int providentFund;

	// setters and getters
	public int getBasicPay() {
		return basicPay;
	}

	public void setBasicPay(int basicPay) {
		this.basicPay = basicPay;
	}

	public int getHra() {
		return hra;
	}

	public void setHra(int hra) {
		this.hra = hra;
	}

	public int getStatutoryBonus() {
		return statutoryBonus;
	}

	public void setStatutoryBonus(int statutoryBonus) {
		this.statutoryBonus = statutoryBonus;
	}

	public int getProvidentFund() {
		return providentFund;
	}

	public void setProvidentFund(int providentFund) {
		this.providentFund = providentFund;
	}

	public int getTotalsalary() {
		return totalsalary;
	}

	public void setTotalsalary(int totalsalary) {
		this.totalsalary = totalsalary;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getNumberOfLeaves() {
		return numberOfLeaves;
	}

	public void setNumberOfLeaves(int numberOfLeaves) {
		this.numberOfLeaves = numberOfLeaves;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFromdate() {
		return fromdate;
	}

	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	public String getTodate() {
		return todate;
	}

	public void setTodate(String todate) {
		this.todate = todate;
	}

	public int getTotalNoOfLeaves() {
		return totalNoOfLeaves;
	}

	public void setTotalNoOfLeaves(int totalNoOfLeaves) {
		this.totalNoOfLeaves = totalNoOfLeaves;
	}

}
