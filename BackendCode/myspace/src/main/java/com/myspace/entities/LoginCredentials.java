package com.myspace.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This entity class represents respective table in database and annotated with
 * JPA annotations.
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "login_credentials")
public class LoginCredentials {

	@Id
	@Column
	private int Id;

	@Column
	private String username;

	@Column
	private String password;

	@Column(name = "employee_id")
	private int employeeId;

	// setters and getters
	public int getId() {
		return Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
}