package com.myspace.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myspace.entities.EmployeeDetails;
import com.myspace.entities.EmployeeLeaves;

import com.myspace.entities.LoginCredentials;

import com.myspace.service.EmployeeService;
import com.myspace.util.JwtImpl;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/myspace")
public class EmployeeControllerImpl {

	@Autowired
	private EmployeeService employeeService;

	/**
	 * this method is used for the employee to login into the application based on
	 * username password
	 * 
	 * @param loginCredentials it will fetch the username and password from the
	 *                         object
	 * @return If sucessful login, it returns status code 200. If username does'nt
	 *         exist, it returns statuscode 202. If the password does'nt exist, it
	 *         returns statuscode 201.
	 */
	@PostMapping("/login")
	public ResponseEntity<?> doAuthenticate(@RequestBody LoginCredentials loginCredentials) {
		int empId = employeeService.doLogin(loginCredentials.getUsername(), loginCredentials.getPassword());

		if (empId == -1) {
			// if username wrong it returns 202 status code along with message
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"enter valid username\",\"statuscode\":" + 202 + "}");
		} else if (empId == -2) {
			// if password is wrong it returns 201 status code along with message
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"wrong password\",\"statuscode\":" + 201 + "}");
		} else if (empId == -3) {
			// if password is wrong it returns 201 status code along with message
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"password must contain atleast one special character \",\"statuscode\":" + 203 + "}");
		}
		// if authentication is success it will return empId along with 200 status code
		// and success message.
		String id = "" + empId;

		// if valid user generating token for that user
		String token = JwtImpl.createJWT(id);
		ObjectMapper mapper = new ObjectMapper();
		try {
			token = mapper.writeValueAsString(token);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"login success\",\"statuscode\":" + 200
				+ ",\"empId\":" + empId + ",\"token\":" + token + "}");
	}

	/**
	 * this method is used for the employee to view his profile based on employee_id
	 * 
	 * @param id it uses employee id to view Employee profile
	 * @return it returns HttpStatus OK in success case and HttpStatus NO_CONTENT in
	 *         failure case
	 */
	@GetMapping("/viewprofile/{employee_id}")
	public ResponseEntity<?> getById(@PathVariable("employee_id") int id) {

		EmployeeDetails employeeDetails = employeeService.findById(id);
		if (employeeDetails != null) {
			return new ResponseEntity<EmployeeDetails>(employeeDetails, HttpStatus.OK);
		}

		return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"id dosent exist\",\"statuscode\":" + 201 + "}");

	}

	/**
	 * this method is used for the employee to update his profile based on
	 * 
	 * @param employee_id with employee_id we are displaying all the details of the
	 *                    employee
	 * @returnit returns status code 200 in success case and status code 201 in
	 *           failure case
	 */
	@PutMapping("/updateprofile/{employee_id}")
	public ResponseEntity<?> updateProfileById(@PathVariable("employee_id") int id,
			@RequestBody EmployeeDetails employee) {

		EmployeeDetails employeeDetails = employeeService.update(id, employee);
		if (employeeDetails == null) {
			return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"not updated\",\"statuscode\":" + 201 + "}");
		}

		return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"updated\",\"statuscode\":" + 200 + "}");
	}

	/**
	 * this method is used for the employee to search his profile based on
	 * employee_id and validates token using id and sends response
	 * 
	 * @param employee_id with employee_id we are displaying all the details of the
	 *                    employee
	 * @return it returns status code 200 in success case and status code 201 in
	 *         failure case
	 * 
	 */
	@GetMapping("/searchEmployee/{employee_id}/{search_id}")
	public @ResponseBody ResponseEntity<String> getAll(@PathVariable("employee_id") int employee_id,
			@PathVariable("search_id") int id, @RequestHeader HttpHeaders headers) {

		String token = "";
		List<String> hList = headers.get("Authorization");

		if (hList == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		// getting token from Herder List
		token = hList.get(0);

		System.out.println("token" + token);

		if (JwtImpl.isValidUser(employee_id, token)) {

			List<EmployeeDetails> entityList = employeeService.searchById(id);
			// creating map to store employee information
			Map<String, Object> list = new HashMap<>();

			ObjectMapper objectMapper = new ObjectMapper();

			String entityJson = null;
			if (entityList == null) {
				list.put("status", "No employee is exist");
				list.put("statuscode", 204);
				try {
					entityJson = objectMapper.writeValueAsString(list);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}

				return ResponseEntity.status(HttpStatus.OK).body(entityJson);
				// .body("{\"status\":\"Employee doesn't exist\",\"statuscode\":" + + "}");
			}

			list.put("statuscode", 200);
			list.put("result", entityList);
			try {
				// converting java object into JSON format
				entityJson = objectMapper.writeValueAsString(list);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			// finally returning responce
			return ResponseEntity.status(HttpStatus.OK).body(entityJson);
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
	}

	/**
	 * This method is used to apply for leaves from-date to to-date
	 * 
	 * @param employee_id with employee_id we are displaying all the details of the
	 *                    employee
	 * @return it returns status code 200 in success case and status code 201 in
	 *         failure case
	 */
	@PostMapping("/leavedetails/{employee_id}")
	public ResponseEntity<?> applyForLeave(@PathVariable("employee_id") int id, @RequestBody EmployeeLeaves employee) {
		EmployeeLeaves employeeLeaves = employeeService.applyForLeave(id, employee);

		if (employeeLeaves.getTotalNoOfLeaves() >= 7 || employeeLeaves.getTotalNoOfLeaves() == 0) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"cannot apply for leave\",\"statuscode\":" + 201 + "}");
		}

		return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"leave applied\",\"statuscode\":" + 200 + "}");

	}

	/**
	 * This method is used to generate salary for the Employee
	 * 
	 * @param employeeId with employee_id we are displaying all the details of the
	 *                   employee
	 * @return it returns status code OK in both cases
	 */
	@GetMapping("/salarydetails/{employee_id}")
	public ResponseEntity<?> salaryDetails(@PathVariable("employee_id") int employeeId) {
		EmployeeLeaves employeeLeaves = employeeService.getSalaryDetails(employeeId);

		if (employeeLeaves == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Salary Generated\",\"statuscode\":" + 200 + "}");
		}

		return new ResponseEntity<EmployeeLeaves>(employeeLeaves, HttpStatus.OK);
	}

	/**
	 * this method is used for the employee to search his profile based on
	 * employee_name
	 * 
	 * @param employee_id with employee_id we are displaying all the details of the
	 *                    employee
	 * @return it returns object in success case and status code 201 in failure case
	 */
	@GetMapping("/searchEmployeeByName/{name}")
	public @ResponseBody ResponseEntity<?> getByName(@PathVariable("name") String name) {
		EmployeeDetails employeeDetails = employeeService.findUserByName(name);

		if (employeeDetails != null) {

			return new ResponseEntity<>(employeeDetails, HttpStatus.OK);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body("{\"status\":\"Name not exists\",\"statuscode\":" + 201 + "}");
	}

	/**
	 * this method is used for the employee to search his profile based on
	 * companyEmailId
	 * 
	 * @param companyEmailId with emailId we are displaying all the details of the
	 * 
	 * 
	 *                       employee
	 * @return it returns object in success case and status code 201 in failure case
	 */
	@GetMapping("/searchEmployeeByEmailId/{companyEmailId}")
	public @ResponseBody ResponseEntity<?> getByEmailId(@PathVariable("companyEmailId") String companyEmailId) {
		EmployeeDetails employeeDetails = employeeService.findUserByEmailId(companyEmailId);

		if (employeeDetails != null) {

			return new ResponseEntity<>(employeeDetails, HttpStatus.OK);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body("{\"status\":\"email not exists\",\"statuscode\":" + 201 + "}");
	}

	/**
	 * this method is used to change the password of an Employee
	 * 
	 * @param id
	 * @param password
	 * @param new_password
	 * @param confirm_new_password
	 * @param headers
	 * @return If both new and old passwords are same, it returns status code 200.
	 *         If username does'nt exist, it returns statuscode 20. If the password
	 *         you entered is not matched, it returns statuscode 204.
	 */
	@PutMapping("/changepassword/{employeeId}/{password}/{new_password}/{confirm_new_password}")
	public ResponseEntity<?> changePassword(@PathVariable("employeeId") int id,
			@PathVariable("password") String password, @PathVariable("new_password") String new_password,
			@PathVariable("confirm_new_password") String confirm_new_password, @RequestHeader HttpHeaders headers) {

		String token = "";
		ObjectMapper mapper = new ObjectMapper();
		String jsonResponce = null;
		Map<String, Object> projectsMap = new HashMap<>();
		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

		token = hList.get(0);

		if (JwtImpl.isValidUser(id, token)) {

			// it will call searchPassword of the employeeService class
			LoginCredentials loginCredentials = employeeService.searchPassword(password);
			// if the object is not null then it will return LoginCredentials object
			if (loginCredentials != null) {
				// if password is existed
				projectsMap.put("status", "not entersed");
				// if both new password are matched then it will update
				if (new_password.equals(confirm_new_password)) {

					loginCredentials.setPassword(new_password);
					employeeService.updatePassword(loginCredentials);
					projectsMap.put("status", 200);
					projectsMap.put("password_updated", loginCredentials.getPassword());

					try {
						jsonResponce = mapper.writeValueAsString(projectsMap);
					} catch (JsonProcessingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);

				}
				// if both new_password and confirm passwords are not matched
				else {

					projectsMap.put("status", "password you entered is not matched");
					projectsMap.put("statuscode", 204);
					try {
						jsonResponce = mapper.writeValueAsString(projectsMap);
					} catch (JsonProcessingException e) {
						e.printStackTrace();

					}
					return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);
				}

			} else {
				projectsMap.put("status", "password is doesn't exist");
				projectsMap.put("statuscode", 204);
				try {
					jsonResponce = mapper.writeValueAsString(projectsMap);
				} catch (JsonProcessingException e) {
					e.printStackTrace();

				}
				return ResponseEntity.status(HttpStatus.OK).body(jsonResponce);

			}
		}

		else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	}
}
