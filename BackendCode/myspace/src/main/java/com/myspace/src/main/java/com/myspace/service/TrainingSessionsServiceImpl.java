package com.myspace.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.myspace.dao.TrainingSessionsDao;

import com.myspace.entities.TrainingSessions;

/**
 * This is the implementation class for TrainingSessionsService
 * 
 * @author IMVIZAG
 *
 */
@Service
public class TrainingSessionsServiceImpl implements TrainingSessionsService {

	// creating the object for TrainingSessionsDao using stereotype annotation
	@Autowired
	TrainingSessionsDao trainingSessions;

	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	/**
	 * this method calls the DAO package for displaying the training sessions
	 */
	@Override
	public List<TrainingSessions> findAll() {
		List<TrainingSessions> trainingSession = new ArrayList<TrainingSessions>();
		Iterable<TrainingSessions> iAccount = trainingSessions.findAll();
		Iterator<TrainingSessions> iterator = iAccount.iterator();

		while (iterator.hasNext()) {
			trainingSession.add(iterator.next());
		}
		return trainingSession;
	}

	/**
	 * this method calls the DAO package for deleting the training sessions
	 */
	@Override
	public boolean deleteEvent(int domainId) {
		boolean flag = false;
		Optional<TrainingSessions> trainingSession = null;
		try {
			trainingSession = trainingSessions.findById(domainId);
		} catch (Exception e) {
			if (trainingSession != null) {
				trainingSessions.delete(trainingSession.get());
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * this method calls the DAO package for finding the session based on the id
	 */
	@Override
	public TrainingSessions findTrainingById(int domainId) {
		try {
			Optional<TrainingSessions> trainingSession = trainingSessions.findById(domainId);
			return trainingSession.get();
		} catch (Exception e) {
			return null;
		}

	}

}
