package com.myspace.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myspace.dao.EventDao;
import com.myspace.entities.CulturalEvents;

/**
 * This is the implementation class for EventService and calls the method from
 * the DAO Package
 * 
 * @author IMVIZAG
 *
 */

@Service
public class EventServiceImpl implements EventService {

	// creating the object for EventDao using stereotype annotation
	@Autowired
	EventDao eventDao;

	/**
	 * this method calls the DAO package for displaying the cultural events
	 */
	@Override
	public List<CulturalEvents> findAll() {
		List<CulturalEvents> culturalEvents = new ArrayList<CulturalEvents>();
		Iterable<CulturalEvents> iAccount = eventDao.findAll();
		Iterator<CulturalEvents> iterator = iAccount.iterator();

		while (iterator.hasNext()) {
			culturalEvents.add(iterator.next());
		}
		return culturalEvents;
	}

	/**
	 * this method calls the DAO package for finding the events based on the eventId
	 * 
	 */
	@Override
	public CulturalEvents findEventById(int eventId) {

		try {
			Optional<CulturalEvents> culturalEvents = eventDao.findById(eventId);
			return culturalEvents.get();
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * this method calls the DAO package for deleting the cultural events
	 */
	@Override
	public boolean deleteEvent(int eventId) {
		Boolean flag = false;
		CulturalEvents culturalEvents = null;
		try {
			culturalEvents = eventDao.findById(eventId).get();

		} catch (Exception e) {
			culturalEvents = null;
		}
		if (culturalEvents != null) {
			eventDao.delete(culturalEvents);
			flag = true;
		}

		return flag;

	}

}
