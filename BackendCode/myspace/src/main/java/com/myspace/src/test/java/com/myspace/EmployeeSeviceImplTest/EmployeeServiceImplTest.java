package com.myspace.EmployeeSeviceImplTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.AssertTrue;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;

import com.myspace.dao.EmployeeDao;
import com.myspace.dao.EmployeeLeavesDao;
import com.myspace.dao.LoginDao;
import com.myspace.entities.EmployeeDetails;
import com.myspace.entities.EmployeeLeaves;
import com.myspace.entities.LoginCredentials;
import com.myspace.service.EmployeeService;
import com.myspace.service.EmployeeServiceImpl;

import static org.mockito.Mockito.*; 
import java.util.ArrayList; 
import java.util.List;
@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class EmployeeServiceImplTest {

@Mock
private LoginDao logindao;

@Mock
private EmployeeDao employeeDao;

@Mock
private EmployeeLeavesDao leavesDao;

@InjectMocks
private EmployeeServiceImpl service;


/**
 * This method gets employee_id from database including all the fields
 */
@Test
public void testFindById_returnsEmployeeDetails() {
  EmployeeDetails emp=new EmployeeDetails();
  emp.setCompanyEmailId("ygas");
  emp.setEmploymentType("jon");
  emp.setWorkLocation("vizag");
	Mockito.<Optional<EmployeeDetails>>when(employeeDao.findById(10334)).thenReturn(Optional.of(emp));
   assertNotNull(service.findById(10334));

}

/**
 * This method will fetch all the profile details based on id and returns the
 * list of employee_details
 */
@Test
public void testSearchById_returnsEmployeeProfile() {
	 EmployeeDetails emp=new EmployeeDetails();
	  emp.setCompanyEmailId("ygas");
	  emp.setEmploymentType("jon");
	  emp.setWorkLocation("vizag");
		Mockito.<Optional<EmployeeDetails>>when(employeeDao.findById(10342)).thenReturn(Optional.of(emp));
		   assertNotNull(service.findById(10342));

}

/**
 * This method takes employee object and id based on the employee_details and
 * modifies the employee details of particular id
 */
@Test
public void testUpdate() {

		LoginCredentials details = new LoginCredentials();
		details.setEmployeeId(10375);
		EmployeeDetails empdetails = new EmployeeDetails();
		empdetails.setAlternativeMobileNumber("987458984");
		
		empdetails.setCompetency("jfnd");
		empdetails.setPermanentLocation("vizag");
		empdetails.setPersonalEmailId("prashanth44551@gmail.com");
		empdetails.setPrimaryMobileNumber("9534872435");
		empdetails.setWorkLocation("vizag");
		empdetails.setWorkPlacePhoneNumber("986634231");
		Mockito.<Optional<EmployeeDetails>>when(employeeDao.findById(10375)).thenReturn(Optional.of(empdetails));
		when(employeeDao.save(empdetails)).thenReturn(empdetails);
		assertNotNull(service.update(10375, empdetails));

	}



/**
 * This method is used to get the salaryDetails
 */
@Test
public void testSalaryDetails_returnsSalaryDetails() {
	
	 EmployeeLeaves employeeLeaves = new EmployeeLeaves();
	 employeeLeaves.setTotalsalary(employeeLeaves.getBasicPay() + employeeLeaves.getHra()
			+ employeeLeaves.getProvidentFund() + employeeLeaves.getStatutoryBonus());
		Mockito.<Optional<EmployeeLeaves>>when(leavesDao.findById(10342)).thenReturn(Optional.of(employeeLeaves));
		   assertNotNull(service.getSalaryDetails(10342));
      }

/**
 * This method is used to apply the leave by calling the method from dao package
 */
@Test
public void testApplyLeave() {

EmployeeLeaves employeeLeaves=new EmployeeLeaves();
		employeeLeaves.setTodate("12-2-2019");
		employeeLeaves.setFromdate("14-2-2019");

		employeeLeaves.setReason("fever");
		employeeLeaves.setLeaveType("casual");

		Mockito.<Optional<EmployeeLeaves>>when(leavesDao.findById(10335)).thenReturn(Optional.of(employeeLeaves));
		when(leavesDao.save(employeeLeaves)).thenReturn(employeeLeaves);
		assertNotNull(service.applyForLeave(10335, employeeLeaves));

}


/**
 * this method is used to get the details of Employee based on employee name
 */
@Test
public void testFindByName_returnsDetailsByName() {

	 EmployeeDetails emp=new EmployeeDetails();
	  emp.setCompanyEmailId("ygas");
	  emp.setEmploymentType("jon");
	  emp.setWorkLocation("vizag");
	  Mockito.<Optional<EmployeeDetails>>when(employeeDao.findByName("pk")).thenReturn(Optional.of(emp));
	  assertNotNull(service.findUserByName("pk"));
}

/**
 * this method is used to get the details of Employee based on employee
 * companyMailId
 */
@Test
public void testFindByEmailId_returnsDetailsByEmailId() {
	 EmployeeDetails emp=new EmployeeDetails();
	  emp.setCompanyEmailId("ygas");
	  emp.setEmploymentType("john");
	  emp.setWorkLocation("vizag");
	  emp.setPersonalEmailId("prashanth44551@gmail.com");
	  emp.setDesignation("javadeveloper");
	  Mockito.<Optional<EmployeeDetails>>when(employeeDao.findBycompanyEmailId("prashanth@innominds.com")).thenReturn(Optional.of(emp));
	  assertNotNull(service.findUserByEmailId("prashanth@innominds.com"));

}

/**
 * this method is used to change the password of an Employee
 */
@Test
public void testUpdatePassword() {
	LoginCredentials details = new LoginCredentials();
	details.setPassword("prashanth");
	Mockito.<Optional<LoginCredentials>>when(logindao.findById(10375)).thenReturn(Optional.of(details));
	when(logindao.save(details)).thenReturn(details);
     assertEquals("prashanth",details.getPassword());

}

///**
// * this method is used to search the password of an Employee
// */
//@Test
//public void testPassword() {
//	LoginCredentials logindetails =new LoginCredentials();
//			logindao.findBypassword(password);
//	return login;
//}
    }
	

