package com.myspace.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This is the entity class in which we can create database tables and generate
 * setters and getters
 * 
 * @author IMVIZAG
 *
 */

@Entity
//it represents the database table
@Table(name = "cultural_events")
public class CulturalEvents {

	// @Id represents the primary key attribute
	@Id
	@Column(name = "event_id")
	private int eventId;
	// it represents the database column
	@Column
	private String eventName;
	@Column
	private String description;
	@Column
	private Date startDate;
	@Column
	private Date endDate;

	// setters and getters
	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
