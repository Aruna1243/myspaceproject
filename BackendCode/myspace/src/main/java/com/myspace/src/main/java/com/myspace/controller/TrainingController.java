package com.myspace.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myspace.entities.TrainingSessions;
import com.myspace.service.TrainingSessionsService;

@RestController
@RequestMapping("/myspace")
@CrossOrigin(origins = "*")
public class TrainingController {

	// creating the object for TrainingSessionsService using stereotype annotation
	@Autowired
	TrainingSessionsService trainingSession;

	/**
	 * this methods calls the service package for displaying all the training
	 * sessions
	 * 
	 * @return
	 */
	@GetMapping("/trainingSession")
	public ResponseEntity<List<TrainingSessions>> list() {
		List<TrainingSessions> trainingSessions = trainingSession.findAll();
		if (trainingSessions.isEmpty()) {
			return new ResponseEntity<List<TrainingSessions>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<TrainingSessions>>(trainingSessions, HttpStatus.OK);
	}

	/**
	 * this methods calls the service package for deleting the training session
	 * based on domainId
	 * 
	 * @param domainId
	 * @return
	 */
	@DeleteMapping("/trainingSessionDelete/{domain_id}")
	public ResponseEntity<?> delete(@PathVariable("domain_id") int domainId) {
		TrainingSessions trainingSessions = trainingSession.findTrainingById(domainId);
		if (trainingSessions != null) {
			trainingSession.deleteEvent(domainId);
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"trainingSessionId deleted\",\"statuscode\":" + 200 + "}");
		}
		return ResponseEntity.status(HttpStatus.OK)
				.body("{\"status\":\"trainingsession id not exist \",\"statuscode\":" + 204 + "}");
	}

}
