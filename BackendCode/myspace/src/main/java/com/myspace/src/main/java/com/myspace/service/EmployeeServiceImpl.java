package com.myspace.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myspace.dao.EmployeeDao;
import com.myspace.dao.EmployeeLeavesDao;

import com.myspace.dao.LoginDao;

import com.myspace.entities.EmployeeDetails;
import com.myspace.entities.EmployeeLeaves;

import com.myspace.entities.LoginCredentials;

/**
 * This is the implementation class for EmployeeService and calls the method
 * from the DAO Package
 * 
 * @author IMVIZAG
 *
 */

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	public static final SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");

	// creating the objects for DAOPackage classes using stereotype annotation
	@Autowired
	private LoginDao logindao;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private EmployeeLeavesDao leavesDao;
	boolean status=false;
	
	String email = "^[a-zA-Z0-9_+*&-]+(?:\\."+"[a-zA-Z0-9_+*&-]+)*@"+"(?:[a-zA-Z0-9-]+\\.)+[a-z"+"A-Z]{2,7}$";

	/**
	 * This method calls dao method to retrieve records, authenticates and returns
	 * value to controller respectively.
	 */
	@Override
	public int doLogin(String username, String password) { 
		LoginCredentials loginCredentials = logindao.findByUsername(username);

		
		if(username.length() > 6 && password.length()>6)
		{	
			for(int i=0;i<password.length();i++)
		{
			char ch=password.charAt(i);
			//condition for checking the special case character
	if(ch=='$'||ch=='@'||ch=='_'||ch=='%'||ch=='!'||ch=='^'||ch=='?'||ch=='*'||ch=='&'||ch=='('||ch==')'||ch=='<'||ch=='>'||ch=='{'||ch=='}'||ch=='/'||ch=='\\'||ch==';')
		 {
		
			status=true;
			break;
		 }
		}
		if(status) {
	if (loginCredentials == null) {
			// if user name wrong it returns -1
			return -1;
		}
		if (!loginCredentials.getPassword().equals(password)) {
			// if password wrong it returns -2
			return -2;
		} else {
			// if credentials are correct it returns empId
			
			return loginCredentials.getEmployeeId();
		      }
		    }
		return -3;
		  }
		return -2;
        }
	

	/**
	 * This method gets employee_id from database including all the fields
	 */
	@Override
	public EmployeeDetails findById(int employeeId) {

		try {
			return employeeDao.findById(employeeId).get();
		}

		catch (Exception e) {
			return null;
		}
	}

	/**
	 * This method will fetch all the profile details based on id and returns the
	 * list of employee_details
	 */
	@Override
	public List<EmployeeDetails> searchById(int id) {
		try {
			Optional<EmployeeDetails> employeeDetails = employeeDao.findById(id);
			List<EmployeeDetails> emp = new ArrayList<EmployeeDetails>();
			emp.add(employeeDetails.get());
			return emp;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * This method takes employee object and id based on the employee_details and
	 * modifies the employee details of particular id
	 */
	@Override
	public EmployeeDetails update(int employeeId, EmployeeDetails employee) {
		try {
			EmployeeDetails employeeDetails = employeeDao.findById(employeeId).get();
			if (employee.getAlternativeMobileNumber().length()==10) {
				employeeDetails.setAlternativeMobileNumber(employee.getAlternativeMobileNumber());
			}
			if (employee.getCompetency() != null) {
				employeeDetails.setCompetency(employee.getCompetency());
			}
			if (employee.getPermanentLocation() != null) {
				employeeDetails.setPermanentLocation(employee.getPermanentLocation());
			}
			Pattern pat = Pattern.compile(email);
		    if(pat.matcher(employee.getPersonalEmailId()).matches()) {
				employeeDetails.setPersonalEmailId(employee.getPersonalEmailId());
			}
			if (employee.getPrimaryMobileNumber().length()==10) {
				employeeDetails.setPrimaryMobileNumber(employee.getPrimaryMobileNumber());
			}
			if (employee.getWorkLocation() != null) {
				employeeDetails.setWorkLocation(employee.getWorkLocation());
			}
			if (employee.getWorkPlacePhoneNumber().length()==10) {
				employeeDetails.setWorkPlacePhoneNumber(employee.getWorkPlacePhoneNumber());
			}
			employeeDao.save(employeeDetails);  
			return employeeDao.findById(employeeId).get();
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * This method is used to get the salaryDetails
	 */
	@Override
	public EmployeeLeaves getSalaryDetails(int employeeId) {
		 EmployeeLeaves employeeLeaves = leavesDao.findById(employeeId).get();
		 employeeLeaves.setTotalsalary(employeeLeaves.getBasicPay() + employeeLeaves.getHra()
				+ employeeLeaves.getProvidentFund() + employeeLeaves.getStatutoryBonus());
		 return leavesDao.findById(employeeId).get();
	      }

	/**
	 * This method is used to apply the leave by calling the method from dao package
	 */
	@Override
	public EmployeeLeaves applyForLeave(int id, EmployeeLeaves leaves) {

		EmployeeLeaves employeeLeaves = leavesDao.findById(id).get();
		int noOfleaves = 0;
		try {
			Date dateBefore = myFormat.parse(leaves.getFromdate());
			Date dateAfter = myFormat.parse(leaves.getTodate());
			long difference = dateAfter.getTime() - dateBefore.getTime();
			float daysBetween = (difference / (1000 * 60 * 60 * 24));
			noOfleaves = Math.round(daysBetween) + 1;
		   } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int maxLeaves = employeeLeaves.getTotalNoOfLeaves()+noOfleaves;

		if (maxLeaves >= 7 || maxLeaves < 0) {
			employeeLeaves.setTotalsalary(employeeLeaves.getBasicPay() + employeeLeaves.getHra()
					+ employeeLeaves.getProvidentFund() + employeeLeaves.getStatutoryBonus());
		} else {
		
			employeeLeaves.setTodate(leaves.getTodate());
			employeeLeaves.setFromdate(leaves.getFromdate());

			employeeLeaves.setReason(leaves.getReason());
			employeeLeaves.setLeaveType(leaves.getLeaveType());
			if (leaves.getLeaveType().equals("sick")  && (employeeLeaves.getBasicPay() - (noOfleaves * 500))>0) {
				employeeLeaves.setNumberOfLeaves(noOfleaves);
				employeeLeaves.setTotalNoOfLeaves(noOfleaves + employeeLeaves.getTotalNoOfLeaves());
				employeeLeaves.setBasicPay(employeeLeaves.getBasicPay() - (noOfleaves * 500));
				employeeLeaves
						.setTotalsalary(employeeLeaves.getBasicPay() - (noOfleaves * 500) + employeeLeaves.getHra()
								+ employeeLeaves.getProvidentFund() + employeeLeaves.getStatutoryBonus());
			}


			leavesDao.save(employeeLeaves);
			return employeeLeaves;

		}
		return leaves;
	}
	

	/**
	 * this method is used to get the details of Employee based on employee name
	 */
	@Override
	public EmployeeDetails findUserByName(String name) {
		try {
			return employeeDao.findByName(name).get();
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * this method is used to get the details of Employee based on employee
	 * companyMailId
	 */
	@Override
	public EmployeeDetails findUserByEmailId(String companyEmailId) {
		try {
			return employeeDao.findBycompanyEmailId(companyEmailId).get();
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * this method is used to change the password of an Employee
	 */
	@Override
	public void updatePassword(LoginCredentials loginCredentials) {
		logindao.save(loginCredentials);

	}

	/**
	 * this method is used to search the password of an Employee
	 */
	@Override
	public LoginCredentials searchPassword(String password) {
		LoginCredentials login = logindao.findBypassword(password);
		return login;
	}
}
