package com.myspace.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;

import com.myspace.dao.EmployeeDao;
import com.myspace.dao.EmployeeLeavesDao;
import com.myspace.dao.LoginDao;
import com.myspace.entities.EmployeeDetails;
import com.myspace.entities.EmployeeLeaves;
import com.myspace.entities.LoginCredentials;
import com.myspace.service.EmployeeServiceImpl;

@SpringBootTest
@RunWith(MockitoJUnitRunner.Silent.class)
public class EmployeeTest {

	@Mock
	private LoginDao logindao;

	@Mock
	private EmployeeDao employeeDao;

	@Mock
	private EmployeeLeavesDao employeeLeavesDao;

	@InjectMocks
	private EmployeeServiceImpl employeeServiceImpl;

	/**
	 * This is the positive test case method for finding the employee based on id
	 */
	@Test
	public void testFindByIdPositive() {
		EmployeeDetails emp = new EmployeeDetails();
		emp.setCompanyEmailId("harshitha@gmail.com");
		emp.setEmploymentType("engineer");
		emp.setWorkLocation("vizag");
		when(employeeDao.findById(10334)).thenReturn(Optional.of(emp));
		assertNotNull(employeeServiceImpl.findById(10334));

	}

	/**
	 * This is the negative test case method for finding the employee based on id
	 */
	@Test
	public void testFindByIdNegative() {
		EmployeeDetails emp = new EmployeeDetails();
		emp.setCompanyEmailId("harshitha@gmail.com");
		emp.setEmploymentType("engineer");
		emp.setWorkLocation("vizag");
		when(employeeDao.findById(10334)).thenReturn(Optional.of(emp));
		assertNull(employeeServiceImpl.findById(10335));

	}

	/**
	 * This is the positive test case method for searching the employee based on id
	 */
	@Test
	public void testSearchByIdPositive() {
		EmployeeDetails emp = new EmployeeDetails();
		emp.setCompanyEmailId("harshitha@gmail.com");
		emp.setEmploymentType("engineer");
		emp.setWorkLocation("vizag");
		Mockito.<Optional<EmployeeDetails>>when(employeeDao.findById(10342)).thenReturn(Optional.of(emp));
		assertNotNull(employeeServiceImpl.findById(10342));

	}

	/**
	 * This is the negative test case method for searching the employee based on id
	 */
	@Test
	public void testSearchByIdNegative() {
		EmployeeDetails emp = new EmployeeDetails();
		emp.setCompanyEmailId("harshitha@gmail.com");
		emp.setEmploymentType("engineer");
		emp.setWorkLocation("hyderabad");
		Mockito.<Optional<EmployeeDetails>>when(employeeDao.findById(10342)).thenReturn(Optional.of(emp));
		assertNull(employeeServiceImpl.findById(10343));

	}

	/**
	 * This is the positive test case method for updating the fields of employee
	 */
	@Test
	public void testUpdatePositive() {

		LoginCredentials loginCredentials = new LoginCredentials();
		loginCredentials.setEmployeeId(10375);
		EmployeeDetails empdetails = new EmployeeDetails();
		empdetails.setAlternativeMobileNumber("987458984");

		empdetails.setCompetency("active");
		empdetails.setPermanentLocation("hyd");
		empdetails.setPersonalEmailId("harshitha@gmail.com");
		empdetails.setPrimaryMobileNumber("9534872435");
		empdetails.setWorkLocation("hyd");
		empdetails.setWorkPlacePhoneNumber("986634231");
		Mockito.<Optional<EmployeeDetails>>when(employeeDao.findById(10375)).thenReturn(Optional.of(empdetails));
		when(employeeDao.save(empdetails)).thenReturn(empdetails);
		assertNotNull(employeeServiceImpl.update(10375, empdetails));

	}

	/**
	 * This is the negative test case method for updating the fields of employee
	 */
	@Test
	public void testUpdateNegative() {

		LoginCredentials loginCredentials = new LoginCredentials();
		loginCredentials.setEmployeeId(10375);
		EmployeeDetails empdetails = new EmployeeDetails();
		empdetails.setAlternativeMobileNumber("987458984");

		empdetails.setCompetency("active");
		empdetails.setPermanentLocation("vizag");
		empdetails.setPersonalEmailId("prashanth44551@gmail.com");
		empdetails.setPrimaryMobileNumber("9534872435");
		empdetails.setWorkLocation("vizag");
		empdetails.setWorkPlacePhoneNumber("986634231");
		Mockito.<Optional<EmployeeDetails>>when(employeeDao.findById(10375)).thenReturn(Optional.of(empdetails));
		when(employeeDao.save(empdetails)).thenReturn(empdetails);
		assertNull(employeeServiceImpl.update(10374, empdetails));

	}

	/**
	 * This is the positive test case method for salaryDetails
	 */
	@Test
	public void testSalaryDetailsPositive() {

		EmployeeLeaves employeeLeaves = new EmployeeLeaves();
		employeeLeaves.setTotalsalary(employeeLeaves.getBasicPay() + employeeLeaves.getHra()
				+ employeeLeaves.getProvidentFund() + employeeLeaves.getStatutoryBonus());
		Mockito.<Optional<EmployeeLeaves>>when(employeeLeavesDao.findById(10342))
				.thenReturn(Optional.of(employeeLeaves));
		assertNotNull(employeeServiceImpl.getSalaryDetails(10342));
	}

	/**
	 * This is the negative test case method for salaryDetails
	 */
	@Test
	public void testSalaryDetailsNegative() {

		EmployeeLeaves employeeLeaves = new EmployeeLeaves();
		employeeLeaves.setTotalsalary(employeeLeaves.getBasicPay() + employeeLeaves.getHra()
				+ employeeLeaves.getProvidentFund() + employeeLeaves.getStatutoryBonus());
		Mockito.<Optional<EmployeeLeaves>>when(employeeLeavesDao.findById(10352))
				.thenReturn(Optional.of(employeeLeaves));
		assertNotNull(employeeServiceImpl.getSalaryDetails(10352));
	}

	/**
	 * This is the positive test case method for applyingLeave
	 */
	@Test
	public void testApplyLeavePositive() {

		EmployeeLeaves employeeLeaves = new EmployeeLeaves();
		employeeLeaves.setTodate("12-2-2019");
		employeeLeaves.setFromdate("14-2-2019");
		employeeLeaves.setReason("fever");
		employeeLeaves.setLeaveType("casual");

		Mockito.<Optional<EmployeeLeaves>>when(employeeLeavesDao.findById(10335))
				.thenReturn(Optional.of(employeeLeaves));
		when(employeeLeavesDao.save(employeeLeaves)).thenReturn(employeeLeaves);
		assertNotNull(employeeServiceImpl.applyForLeave(10335, employeeLeaves));

	}

	/**
	 * This is the negative test case method for applyingLeave
	 */
	@Test
	public void testApplyLeaveNegative() {

		EmployeeLeaves employeeLeaves = new EmployeeLeaves();
		employeeLeaves.setTodate("12-2-2019");
		employeeLeaves.setFromdate("14-2-2019");
		employeeLeaves.setReason("fever");
		employeeLeaves.setLeaveType("casual");

		Mockito.<Optional<EmployeeLeaves>>when(employeeLeavesDao.findById(10336))
				.thenReturn(Optional.of(employeeLeaves));
		// when(employeeLeavesDao.save(employeeLeaves)).thenReturn(employeeLeaves);
		// assertNull(employeeServiceImpl.applyForLeave(10336, employeeLeaves));

	}

	/**
	 * This is the positive test case method for salaryDetails
	 */
	@Test
	public void testFindByNamePositive() {

		EmployeeDetails emp = new EmployeeDetails();
		emp.setCompanyEmailId("harshitha@gmail.com");
		emp.setEmploymentType("engineer");
		emp.setWorkLocation("hyderabad");
		Mockito.<Optional<EmployeeDetails>>when(employeeDao.findByName("sita")).thenReturn(Optional.of(emp));
		assertNotNull(employeeServiceImpl.findUserByName("sita"));
	}

	/**
	 * This is the negative test case method for salaryDetails
	 */
	@Test
	public void testFindByNameNegative() {

		EmployeeDetails emp = new EmployeeDetails();
		emp.setCompanyEmailId("harshitha@gmail.com");
		emp.setEmploymentType("engineer");
		emp.setWorkLocation("hyderabad");
		Mockito.<Optional<EmployeeDetails>>when(employeeDao.findByName("sita")).thenReturn(Optional.of(emp));
		assertNull(employeeServiceImpl.findUserByName("rita"));
	}

	/**
	 * This is the positive test case method for finding the employee based on
	 * emailId
	 */
	@Test
	public void testFindByEmailIdPositive() {
		EmployeeDetails emp = new EmployeeDetails();
		emp.setCompanyEmailId("harshitha@gmail.com");
		emp.setEmploymentType("engineer");
		emp.setWorkLocation("hyderabad");
		emp.setPersonalEmailId("harshitha@gmail.com");
		emp.setDesignation("java developer");
		Mockito.<Optional<EmployeeDetails>>when(employeeDao.findBycompanyEmailId("harshitha@gmail.com"))
				.thenReturn(Optional.of(emp));
		assertNotNull(employeeServiceImpl.findUserByEmailId("harshitha@gmail.com"));

	}

	/**
	 * This is the negative test case method for finding the employee based on
	 * emailId
	 */
	@Test
	public void testFindByEmailIdNegative() {
		EmployeeDetails emp = new EmployeeDetails();
		emp.setCompanyEmailId("harshitha@gmail.com");
		emp.setEmploymentType("engineer");
		emp.setWorkLocation("hyderabad");
		emp.setPersonalEmailId("harshitha@gmail.com");
		emp.setDesignation("java developer");
		Mockito.<Optional<EmployeeDetails>>when(employeeDao.findBycompanyEmailId("harshitha@gmail.com"))
				.thenReturn(Optional.of(emp));
		assertNull(employeeServiceImpl.findUserByEmailId("harshi@gmail.com"));

	}

	/**
	 * This is the positive test case method for updating the password
	 */
	@Test
	public void testUpdatePasswordPositive() {
		LoginCredentials details = new LoginCredentials();
		details.setPassword("harshitha");
		Mockito.<Optional<LoginCredentials>>when(logindao.findById(10375)).thenReturn(Optional.of(details));
		when(logindao.save(details)).thenReturn(details);
		assertEquals("harshitha", details.getPassword());

	}

///**
// * this method is used to search the password of an Employee 
// */
//@Test
//public void testPassword() {
//	LoginCredentials logindetails =new LoginCredentials();
//			logindao.findBypassword(password);
//	return login;
//}
}
