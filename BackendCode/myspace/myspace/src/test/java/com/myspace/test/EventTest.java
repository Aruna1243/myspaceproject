package com.myspace.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.myspace.dao.EventDao;
import com.myspace.entities.CulturalEvents;
import com.myspace.service.EventServiceImpl;

@SpringBootTest
@RunWith(MockitoJUnitRunner.Silent.class)
public class EventTest {

	@Mock
	EventDao eventDao;

	@InjectMocks
	EventServiceImpl eventService;

	@Test
	public void displayCulturalEventsPositive() {

		List<CulturalEvents> list = new ArrayList<CulturalEvents>();

		CulturalEvents culturalEvents1 = new CulturalEvents();
		culturalEvents1.setEventId(104);
		culturalEvents1.setEventName("games");
		culturalEvents1.setDescription("tabletennis");
		culturalEvents1.setStartDate("2019-6-26");
		culturalEvents1.setEndDate("2019-7-26");

		CulturalEvents culturalEvents2 = new CulturalEvents();
		culturalEvents2.setEventId(105);
		culturalEvents2.setEventName("games");
		culturalEvents2.setDescription("football");
		culturalEvents2.setStartDate("2019-5-26");
		culturalEvents2.setEndDate("2019-6-26");

		// Mockito.<Optional<CulturalEvents>>when(eventDao.findById(104)).thenReturn(Optional.of(culturalEvents1));
		when(eventDao.findAll()).thenReturn(list);
		Assert.assertTrue(eventService.findAll().isEmpty());

	}

	@Test
	public void displayCulturalEventsNegative() {

		List<CulturalEvents> list = new ArrayList<CulturalEvents>();

		CulturalEvents culturalEvents1 = new CulturalEvents();
		culturalEvents1.setEventId(104);
		culturalEvents1.setEventName("games");
		culturalEvents1.setDescription("tabletennis");
		culturalEvents1.setStartDate("2019-6-26");
		culturalEvents1.setEndDate("2019-7-26");

		CulturalEvents culturalEvents2 = new CulturalEvents();
		culturalEvents2.setEventId(105);
		culturalEvents2.setEventName("games");
		culturalEvents2.setDescription("football");
		culturalEvents2.setStartDate("2019-5-26");
		culturalEvents2.setEndDate("2019-6-26");

		when(eventDao.findAll()).thenReturn(list);
		Assert.assertFalse(!eventService.findAll().isEmpty());

	}

	@Test
	public void findEventByIdPositive() {

		CulturalEvents culturalEvents1 = new CulturalEvents();
		culturalEvents1.setEventId(104);
		culturalEvents1.setEventName("games");
		culturalEvents1.setDescription("tabletennis");
		culturalEvents1.setStartDate("2019-6-26");
		culturalEvents1.setEndDate("2019-7-26");

		when(eventDao.findById(104)).thenReturn(Optional.of(culturalEvents1));
		assertTrue(!eventService.findEventById(104).equals(null));

	}

	@Test
	public void findEventByIdNegative() {

		CulturalEvents culturalEvents1 = new CulturalEvents();
		culturalEvents1.setEventId(104);
		culturalEvents1.setEventName("games");
		culturalEvents1.setDescription("tabletennis");
		culturalEvents1.setStartDate("2019-6-26");
		culturalEvents1.setEndDate("2019-7-26");

		when(eventDao.findById(104)).thenReturn(Optional.of(culturalEvents1));
		assertFalse(eventService.findEventById(104).equals(null));

	}

	@Test
	public void deleteEventByIdPositive() {

		CulturalEvents culturalEvents1 = new CulturalEvents();
		culturalEvents1.setEventId(105);
		culturalEvents1.setEventName("games");
		culturalEvents1.setDescription("tabletennis");
		culturalEvents1.setStartDate("2019-6-26");
		culturalEvents1.setEndDate("2019-7-26");
		
		Mockito.<Optional<CulturalEvents>>when(eventDao.findById(105)).thenReturn(Optional.of(culturalEvents1));
		assertTrue(eventService.deleteEvent(105));
		

		
	}

}
