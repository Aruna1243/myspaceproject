package com.myspace.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.myspace.dao.TrainingSessionsDao;
import com.myspace.entities.TrainingSessions;
import com.myspace.service.TrainingSessionsServiceImpl;

@SpringBootTest
@RunWith(MockitoJUnitRunner.Silent.class)
public class TrainingSessionsTest {

	@Mock
	TrainingSessionsDao trainingSessionsDao;

	@InjectMocks
	TrainingSessionsServiceImpl trainingSessionsServiceImpl;

	@Test
	public void displayTrainingSessionsPositive() {

		List<TrainingSessions> list = new ArrayList<TrainingSessions>();
		java.util.Date date = new java.util.Date();
		TrainingSessions trainingSessions1 = new TrainingSessions();
		trainingSessions1.setDomainId(101);
		trainingSessions1.setTitle("Salesforce");
		trainingSessions1.setStart(date);
		trainingSessions1.setEnd(date);

		TrainingSessions trainingSessions2 = new TrainingSessions();
		trainingSessions2.setDomainId(102);
		trainingSessions2.setTitle("BigData");
		trainingSessions2.setStart(date);
		trainingSessions2.setEnd(date);

		// Mockito.<Optional<CulturalEvents>>when(eventDao.findById(104)).thenReturn(Optional.of(culturalEvents1));
		when(trainingSessionsDao.findAll()).thenReturn(list);
		Assert.assertTrue(trainingSessionsServiceImpl.findAll().isEmpty());

	}

	@Test
	public void displayTrainingSessionsNegative() {

		List<TrainingSessions> list = new ArrayList<TrainingSessions>();
		java.util.Date date = new java.util.Date();
		TrainingSessions trainingSessions1 = new TrainingSessions();
		trainingSessions1.setDomainId(101);
		trainingSessions1.setTitle("Salesforce");
		trainingSessions1.setStart(date);
		trainingSessions1.setEnd(date);

		TrainingSessions trainingSessions2 = new TrainingSessions();
		trainingSessions2.setDomainId(102);
		trainingSessions2.setTitle("BigData");
		trainingSessions2.setStart(date);
		trainingSessions2.setEnd(date);

		// Mockito.<Optional<CulturalEvents>>when(eventDao.findById(104)).thenReturn(Optional.of(culturalEvents1));
		when(trainingSessionsDao.findAll()).thenReturn(list);
		Assert.assertFalse(!trainingSessionsServiceImpl.findAll().isEmpty());

	}

	@Test
	public void findSessionByIdPositive() {

		java.util.Date date = new java.util.Date();
		TrainingSessions trainingSessions = new TrainingSessions();
		trainingSessions.setDomainId(101);
		trainingSessions.setTitle("Salesforce");
		trainingSessions.setStart(date);
		trainingSessions.setEnd(date);

		when(trainingSessionsDao.findById(101)).thenReturn(Optional.of(trainingSessions));
		assertTrue(!trainingSessionsServiceImpl.findTrainingById(101).equals(null));

	}

	@Test
	public void findSessionByIdNegative() {

		java.util.Date date = new java.util.Date();
		TrainingSessions trainingSessions = new TrainingSessions();
		trainingSessions.setDomainId(101);
		trainingSessions.setTitle("Salesforce");
		trainingSessions.setStart(date);
		trainingSessions.setEnd(date);

		when(trainingSessionsDao.findById(101)).thenReturn(Optional.of(trainingSessions));
		assertFalse(trainingSessionsServiceImpl.findTrainingById(101).equals(null));

	}
}
