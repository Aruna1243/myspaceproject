package com.myspace.util;

import java.security.Key;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * This Class Generates JWT tokens for Authorization purpose
 * 
 * @author IMVIZAG
 *
 */
public class JwtImpl {

	// The secret key. This should be in a property file NOT under source
	// control and not hard coded in real life. We're putting it here for
	// simplicity.
	private static String SECRET_KEY = "oeRaYY7Wo24sDqKSX3IM9ASGmdGPmkTd9jo1QTy4b7P9Ze5_9hKolVX8xNrQDcNRfVEdTZNOuOyqEGhXEbdJI";

	/**
	 * 
	 * This Method actually generates Jwt Token including Employee ID
	 * 
	 * @param id
	 * @return JWT Token In string format
	 */
	// Sample method to construct a JWT
	public static String createJWT(String id) {
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		// Creating signature based on Hashing Algorithm
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		// Let's set the JWT Claims
		JwtBuilder builder = Jwts.builder().setId(id).setIssuedAt(now).signWith(signatureAlgorithm, signingKey);

//        //if it has been specified, let's add the expiration
//        if (ttlMillis >= 0) {
//            long expMillis = nowMillis + ttlMillis;
//            Date exp = new Date(expMillis);
//            builder.setExpiration(exp);
//        }

		// Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}

	/**
	 * This Method is used to decode The given Token if it received invalid token it
	 * throws Exception
	 * 
	 * @param jwt
	 * @return Claims Object
	 * @throws Exception
	 */
	public static Claims decodeJWT(String jwt) throws Exception {

		// This line will throw an exception if it is not a signed JWS (as expected)
		try {
			Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
					.parseClaimsJws(jwt).getBody();
			return claims;
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * This Method Gets Employee id from user and checks that Id with token
	 * contained ID if match return true otherwise return false
	 * 
	 * @param empId
	 * @param token
	 * @return boolean value
	 */
	public static boolean isValidUser(int employeeId, String token) {
		boolean result = false;
		try {
			// getting Claims object to decode token
			Claims claims = decodeJWT(token);
			String tokenId = "" + employeeId;

			// checking Employee Id with Token Contained employee id
			if (claims.getId().equals(tokenId)) {
				result = true;
			}
		} catch (Exception e) {
			return result;
		}
		// returning boolena value
		return result;
	}
}