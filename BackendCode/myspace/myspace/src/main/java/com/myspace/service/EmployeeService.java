package com.myspace.service;

import java.util.List;

import com.myspace.entities.Employee;
import com.myspace.entities.EmployeeDetails;


/**
 * This is the service interface which contains all the abstract methods.
 * 
 * @author IMVIZAG
 *
 */
public interface EmployeeService {

	public Employee save(Employee loginUtilities);

	public List<EmployeeDetails> findAll();

	public Employee findByUsername(int id);

}
