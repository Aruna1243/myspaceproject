package com.myspace.service;

import java.util.List;

import com.myspace.entities.TrainingSessions;

/**
 * This is the service interface which contains all the abstract methods.
 * 
 * @author IMVIZAG
 *
 */
public interface TrainingSessionsService {

	public List<TrainingSessions> findAll();

	public TrainingSessions findTrainingById(int domainId);

	public boolean deleteEvent(int domainId);

}