package com.myspace.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.Table;

/**
 * This is the entity class in which we can create database tables and generate
 * setters and getters
 * 
 * @author IMVIZAG
 *
 */

@Entity
//it represents the database table
@Table(name = "Employee_Details")
public class EmployeeDetails {

	// @Id represents the primary key attribute
	@Id
	// it represents the database column
	@Column
	private int employee_Id;
	@Column
	private String userName;
	@Column
	private String Department;
	@Column
	private String Designation;
	@Column
	private String worklocation;
	@Column
	private String permanentLocation;
	@Column
	private String personalemailID;
	@Column
	private long workPlacePhoneNumber;
	@Column
	private long primaryMobileNumber;
	@Column
	private long alternativeMobileNumber;
	@Column
	private String companyemailID;
	@Column
	private Date sezJoiningDate;

	// setters and getters
	public int getEmployee_Id() {
		return employee_Id;
	}

	public void setEmployee_Id(int employee_Id) {
		this.employee_Id = employee_Id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDepartment() {
		return Department;
	}

	public void setDepartment(String department) {
		Department = department;
	}

	public String getDesignation() {
		return Designation;
	}

	public void setDesignation(String designation) {
		Designation = designation;
	}

	public String getWorklocation() {
		return worklocation;
	}

	public void setWorklocation(String worklocation) {
		this.worklocation = worklocation;
	}

	public String getPermanentLocation() {
		return permanentLocation;
	}

	public void setPermanentLocation(String permanentLocation) {
		this.permanentLocation = permanentLocation;
	}

	public String getPersonalemailID() {
		return personalemailID;
	}

	public void setPersonalemailID(String personalemailID) {
		this.personalemailID = personalemailID;
	}

	public long getWorkPlacePhoneNumber() {
		return workPlacePhoneNumber;
	}

	public void setWorkPlacePhoneNumber(long workPlacePhoneNumber) {
		this.workPlacePhoneNumber = workPlacePhoneNumber;
	}

	public long getPrimaryMobileNumber() {
		return primaryMobileNumber;
	}

	public void setPrimaryMobileNumber(long primaryMobileNumber) {
		this.primaryMobileNumber = primaryMobileNumber;
	}

	public long getAlternativeMobileNumber() {
		return alternativeMobileNumber;
	}

	public void setAlternativeMobileNumber(long alternativeMobileNumber) {
		this.alternativeMobileNumber = alternativeMobileNumber;
	}

	public String getCompanyemailID() {
		return companyemailID;
	}

	public void setCompanyemailID(String companyemailID) {
		this.companyemailID = companyemailID;
	}

	public Date getSezJoiningDate() {
		return sezJoiningDate;
	}

	public void setSezJoiningDate(Date sezJoiningDate) {
		this.sezJoiningDate = sezJoiningDate;
	}

}
