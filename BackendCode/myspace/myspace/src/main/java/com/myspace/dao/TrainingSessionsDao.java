package com.myspace.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.myspace.entities.TrainingSessions;

/**
 * This is the dao class which extends the crud repository
 * 
 * @author IMVIZAG
 *
 */

@Repository
public interface TrainingSessionsDao extends CrudRepository<TrainingSessions, Integer> {

}
