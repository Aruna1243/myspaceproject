package com.myspace.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.myspace.dao.TrainingSessionsDao;

import com.myspace.entities.TrainingSessions;

@Service
public class TrainingSessionsServiceImpl implements TrainingSessionsService {

	// creating the object for TrainingSessionsDao using stereotype annotation
	@Autowired
	TrainingSessionsDao trainingSessionsDao;

	/**
	 * this method calls the DAO package for displaying the training sessions
	 */
	@Override
	public List<TrainingSessions> findAll() {
		List<TrainingSessions> list = new ArrayList<TrainingSessions>();
		Iterable<TrainingSessions> iAccount = trainingSessionsDao.findAll();
		Iterator<TrainingSessions> iterator = iAccount.iterator();

		while (iterator.hasNext()) {
			list.add(iterator.next());
		}
		return list;
	}

	/**
	 * this method calls the DAO package for deleting the training sessions
	 */
	@Override
	public boolean deleteEvent(int domainId) {
		Optional<TrainingSessions> trainingSessions = trainingSessionsDao.findById(domainId);
		if (trainingSessions != null) {
			trainingSessionsDao.delete(trainingSessions.get());
			return true;
		}
		return false;
	}

	/**
	 * this method calls the DAO package for finding the session based on the id
	 */
	@Override
	public TrainingSessions findTrainingById(int domainId) {
		Optional<TrainingSessions> trainingSessions = trainingSessionsDao.findById(domainId);
		return trainingSessions.get();
	}

}