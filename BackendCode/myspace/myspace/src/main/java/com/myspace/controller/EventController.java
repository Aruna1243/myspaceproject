package com.myspace.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myspace.dao.EventDao;
import com.myspace.entities.CulturalEvents;
import com.myspace.service.EventService;

@RestController
@RequestMapping("/api2")
@CrossOrigin(origins = "*")
public class EventController {

	// creating the object for EventService using stereotype annotation
	@Autowired
	EventService eventService;
	// creating the object for EventDao using stereotype annotation
	@Autowired
	EventDao eventDao;

	/**
	 * this methods calls the service package for displaying all the cultural events
	 * 
	 * @return
	 */
	@GetMapping("/displayEvents")
	public ResponseEntity<List<CulturalEvents>> list() {
		List<CulturalEvents> events = eventService.findAll();
		if (events.isEmpty()) {
			return new ResponseEntity<List<CulturalEvents>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<CulturalEvents>>(events, HttpStatus.OK);
	}

	/**
	 * this methods calls the service package for displaying the cultural events
	 * based on eventId
	 * 
	 * @param eventId
	 * @return
	 */
	@GetMapping("/events/{id}")
	public ResponseEntity<CulturalEvents> get(@PathVariable("eventId") int eventId) {
		CulturalEvents culturalEvents = eventService.findEventById(eventId);
		if (culturalEvents == null) {

			return new ResponseEntity<CulturalEvents>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<CulturalEvents>(culturalEvents, HttpStatus.OK);
	}

	/**
	 * this methods calls the service package for deleting the cultural events based
	 * on eventId
	 * 
	 * @param eventId
	 * @return
	 */
	@DeleteMapping("/events/delete/{event_id}")
	public ResponseEntity<Boolean> delete(@PathVariable("event_id") int eventId) {
		CulturalEvents culturalEvents = eventService.findEventById(eventId);
		if (culturalEvents != null) {
			eventService.deleteEvent(eventId);

			return new ResponseEntity<Boolean>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Boolean>(HttpStatus.NO_CONTENT);
	}
}
