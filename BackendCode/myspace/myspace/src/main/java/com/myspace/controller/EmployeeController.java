package com.myspace.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myspace.entities.Employee;
import com.myspace.service.EmployeeService;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	// creating the object for EmployeeService using stereotype annotation
	@Autowired
	private EmployeeService employeeService;

	/**
	 * This methods calls the service package for saving the Employees
	 * 
	 * @return
	 */
	@PostMapping("/employee/create")
	public ResponseEntity<?> createNewAccount(@RequestBody Employee employee) {
		Employee emp = employeeService.save(employee);

		return new ResponseEntity<>("New account is created with id:" + emp.getMyId(), HttpStatus.CREATED);
	}

	/**
	 * This method is used for the employee to login into the application based on
	 * UserName
	 * 
	 * @param employee
	 * @return
	 */
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody Employee employee) {

		Employee result = employeeService.findByUsername(employee.getMyId());
		if (result.getUserName().equals(employee.getUserName())
				&& result.getPassword().equals(employee.getPassword())) {
			return new ResponseEntity<>("Login Successfull", HttpStatus.OK);
		}
		return new ResponseEntity<>("Login Failed", HttpStatus.NOT_FOUND);

	}

}