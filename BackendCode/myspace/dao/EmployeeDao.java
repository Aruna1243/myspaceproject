package com.myspace.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.myspace.entities.Employee;

/**
 * This is the dao class which extends the crud repository
 * 
 * @author IMVIZAG
 *
 */

@Repository
public interface EmployeeDao extends CrudRepository<Employee, Integer> {

}
