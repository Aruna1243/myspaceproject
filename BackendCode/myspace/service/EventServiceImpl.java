package com.myspace.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myspace.dao.EventDao;
import com.myspace.entities.CulturalEvents;

@Service
public class EventServiceImpl implements EventService {

	// creating the object for EventDao using stereotype annotation
	@Autowired
	EventDao eventDao;

	/**
	 * this method calls the DAO package for displaying the cultural events
	 */
	@Override
	public List<CulturalEvents> findAll() {
		List<CulturalEvents> list = new ArrayList<CulturalEvents>();
		Iterable<CulturalEvents> iAccount = eventDao.findAll();
		Iterator<CulturalEvents> iterator = iAccount.iterator();

		while (iterator.hasNext()) {
			list.add(iterator.next());
		}
		return list;
	}

	/**
	 * this method calls the DAO package for finding the events based on the eventId
	 */
	@Override
	public CulturalEvents findEventById(int eventId) {
		Optional<CulturalEvents> account = eventDao.findById(eventId);

		return account.get();
	}

	/**
	 * this method calls the DAO package for deleting the cultural events
	 */
	@Override
	public boolean deleteEvent(int eventId) {
		Optional<CulturalEvents> details = eventDao.findById(eventId);
		if (details != null) {
			eventDao.delete(details.get());
			return true;
		}
		return false;
	}

}
