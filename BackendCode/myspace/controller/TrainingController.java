package com.myspace.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myspace.entities.TrainingSessions;
import com.myspace.service.TrainingSessionsService;

@RestController
@RequestMapping("/api3")
@CrossOrigin(origins = "*")
public class TrainingController {

	//creating the object for TrainingSessionsService using stereotype annotation
	@Autowired
	TrainingSessionsService trainingSession;

	/**
	 * this methods calls the service package for displaying all the training
	 * sessions
	 * 
	 * @return
	 */
	@GetMapping("/trainingSession")
	public ResponseEntity<List<TrainingSessions>> list() {
		List<TrainingSessions> sessions = trainingSession.findAll();
		if (sessions.isEmpty()) {
			return new ResponseEntity<List<TrainingSessions>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<TrainingSessions>>(sessions, HttpStatus.OK);
	}

	/**
	 * this methods calls the service package for deleting the training
	 * session based on domainId
	 * 
	 * @param domainId
	 * @return
	 */
	@DeleteMapping("/trainingSession/delete/{domain_id}")
	public ResponseEntity<Boolean> delete(@PathVariable("domain_id") int domainId) {
		TrainingSessions sessions = trainingSession.findTrainingById(domainId);
		if (sessions != null) {
			trainingSession.deleteEvent(domainId);

			return new ResponseEntity<Boolean>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Boolean>(HttpStatus.NO_CONTENT);
	}

}
