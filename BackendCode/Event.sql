CREATE TABLE `myspace`.`cultural_events` (
  `event_id` INT,
  `event_name` VARCHAR(45) NULL,
  `description` VARCHAR(500) NULL,
  `start_date` DATE NULL,
  `end_date` DATE NULL,
  PRIMARY KEY (`event_id`));

drop table cultural_events;

SELECT * FROM myspace.cultural_events;

INSERT INTO `myspace`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('1', 'InnomindsBash', 'Innominds friday night party including Feast.', '2019-02-19', '2019-03-24');
INSERT INTO `myspace`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('2', 'HostWorkshop', 'Innominds, in collaboration with the ‘Interaction Design Foundation,’ organized a design workshop on the topic, “Challenges of Designers in Corporates”. ', '2019-02-19', '2019-03-24');
INSERT INTO `myspace`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('3', 'FlashMob', 'Innominds is conducting a flashmob for all the employee\'s who are intrested.', '2019-02-28', '2019-02-28');
INSERT INTO `myspace`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('4', 'BasketBallTournament', 'Innominds is conducting a tournament for the bsketball players', '2019-03--03', '2019-03-03');
INSERT INTO `myspace`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('5', 'TeamOuting', 'Innominds is conducting an outing for employees for every 3 months', '2019-04-04', '2019-07-05');
INSERT INTO `myspace`.`cultural_events` (`event_id`, `event_name`, `description`, `start_date`, `end_date`) VALUES ('6', 'TableTennis', 'TableTennis Tournament every friday', '2019-01-02', '2020-01-01');
