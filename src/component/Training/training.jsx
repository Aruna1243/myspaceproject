import React from 'react';
import Header from '../Header/header'
import LeftMenu from '../NavBar/leftmenu';
import Footer from '../Footer/footer';
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import * as trainingActions from '../../store/actions/trainingActions'
import { bindActionCreators } from 'redux';
import FullCalendar from 'fullcalendar-reactwrapper';
import 'fullcalendar/dist/fullcalendar.css';


class Training extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      start: "",
      end: "",
      event: []
    }
  }
  componentWillReceiveProps(new_props) {
    if (new_props.profile_loaded) {
      console.log(new_props.profile_loaded);
      this.setState({ event: new_props.profile_loaded })
      console.log("Event", this.state.event)
    }


  }
  componentDidMount() {
    debugger
    this.props.trainingActions.fetchTime();
  }

  render() {
    console.log("Event render", this.state.event);
    let startd = this.state.event.map((e) => {
      var start = e.start + "T" + e.startTime
      console.log(start);
    })
    console.log(startd);


    if (localStorage.getItem('token') == null) {
      return (<Redirect to="/" />)
    }
    return (
      <div>
        <Header />
        <div className="row">
          <div className="col-sm-3">
            <LeftMenu />
          </div>
          <div className="col-sm-9">
            <FullCalendar
              id="your-custom-ID"
              header={{
                left: 'prev,next today myCustomButton',
                center: 'title',
                right: 'month,basicWeek,basicDay'
              }}

              timeFormat='H(:mm)a'
              eventColor='#378006'
              displayEventStart={true}
              navLinks={true}
              eventLimit={true}
              events={this.state.event}
              eventClick={function (event) {
              }
              }
            />
          </div>
        </div>
        <div className="col-sm-12 " style={{ marginTop: "0px" }}>
          <Footer />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log(state.trainingReducer.events)
  return {
    profile_loaded: state.trainingReducer.events,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    trainingActions: bindActionCreators(trainingActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Training)