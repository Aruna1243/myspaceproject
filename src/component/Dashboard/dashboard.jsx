import React, { Component } from 'react';
import Header               from '../Header/header'
import NavBar               from '../NavBar/leftmenu'
import Footer               from '../Footer/footer'
import { Redirect }         from 'react-router-dom'

class Dashboard extends Component {

  render() {
    if(localStorage.getItem('token') == null){
      return (<Redirect to="/"/>)
    }
    return (
      <div className="App">
        <Header/>
        <div className="col-sm-3">
          <NavBar/>
        </div>
       
        <div className="col-sm-12 fixed-bottom" style={{marginTop:"0px"}}>
          <Footer/>
        </div>
      </div>
    );
  }
}

export default Dashboard;
