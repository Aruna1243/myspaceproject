import React, { Component } from 'react';
import Header from '../Header/header'
import LeftMenu               from '../NavBar/leftmenu';
import Footer                 from '../Footer/footer';
import { Redirect } from 'react-router-dom'
// import Avatar from '../../assets/images/img.png'
import { Link } from 'react-router-dom';
import * as searchAction from '../../store/actions/searchActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import '../../assets/css/SalaryDetails/salaryDetails.css'
import $ from 'jquery'


class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      details: {},
      searchstring: ''
    }
  }

componentWillMount(){
console.log("ghgdhf")
    const name = this.props.match.params.value
    if(!this.props.match.params.value.includes("@")){
      this.props.searchAction.searchByName(name);
    }
    else{
      this.props.searchAction.searchByEmail(name);
    }
  }

  render() {
    debugger;
    console.log(this.props.details)
    return (
      <div className="App">
          <Header />
        <div className="row">
          <div className="col-sm-3">
            <LeftMenu />
          </div>
          <div className="col-sm-9">
          {
            (this.props.details) ?
              <div>
                <div class="card-container salarycard ">
                  <div class="flip-card-inner">
                    <div class="flip-card-front">
                    <table class="table table-borderless" style={{textAlign:"justify"}}>
                   <tr>
                     <td> EmployeeId:</td>
                    <td><h3>{this.props.details.employeeId}</h3></td>
                    </tr>
                    <tr>
                     <td> Name:</td>
                    <td> <p>{this.props.details.name}</p></td>
                    </tr>
                    <tr>
                      <td> Designation:</td>
                      <td><p>{this.props.details.designation}</p></td>
                    </tr>
                    <tr>
                      <td> PrimaryMobileNumber:</td>
                      <td><p>{this.props.details.primaryMobileNumber}</p></td>
                    </tr>
                    <tr>
                      <td> Department :</td>
                      <td><p>{this.props.details.department}</p></td>
                    </tr>
                    <tr>
                      <td> workLocatiuon :</td>
                      <td><p>{this.props.details.workLocation}</p></td>
                    </tr>
                    <tr>
                      <td> Comapany :</td>
                      <td><p>{this.props.details.companyEmailId}</p></td>
                    </tr>
                    </table>
                    </div>
                  </div>
                </div>
                {/* {this.props.details.employeeId}
                {this.props.details.employeeId} */}
              </div> :
              <div style={{color:"#78b5ba"}}>No such email or id presnt in this</div>
          }
          {/* {this.props.details.employeeId} */}
        </div>
        </div>

        <div className="col-sm-12" style={{ marginTop: "0px" }}>

        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  debugger;
  console.log(state.searchreducer.statuscode)
  return {
    details: state.searchreducer.details,
    statuscode:state.searchreducer.statuscode
  }
}

function mapDispatchToProps(dispatch) {
  return {
    searchAction: bindActionCreators(searchAction, dispatch),

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)