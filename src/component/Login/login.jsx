import React, { Component } 		from 'react';
import 							 	 							 '../../assets/css/Login/login.css'
import * as authActions 				from '../../store/actions/authActions'
import { connect } 							from 'react-redux'
import { Redirect } 						from 'react-router-dom';
import { bindActionCreators } 	from 'redux';
import 																'../../assets/css/Login/login.css'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
			username:'',
			password:'',
			isSubmitted:false
		}

		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);

	}

	onChange(e){
		this.setState({
			[e.target.name]:e.target.value
		});
	}
	
	onSubmit(e){
		debugger;
		e.preventDefault();
		const data = {
			username : this.state.username,
			password : this.state.password
		}
		this.props.authActions.doLoginUser(data);
		this.setState({
			isSubmitted: true
		})
		
		
	}

    render() {
			if(localStorage.getItem('token')){
				debugger;
				return(<Redirect to="/dashboard"/>);
			}
			if(this.props.statuscode == 202){
				debugger;
				alert("Username does not exist");
				return  (window.location.href = '/')
			}
			else if(this.props.statuscode == 201){
				debugger;
				alert("Check your password");
				return  (window.location.href = '/')
			}
        return (
			<div className="body" >
				<h1 className="heading">
					<span>M</span>y
					<span>S</span>pace
					<span>S</span>ign
					<span>I</span>n
					<span>F</span>orm
				</h1>
				<div class="sub-main-w3">
					<form onSubmit={this.onSubmit}>
						<div class="form-style-agile">
							<label>UserName<i class="fas fa-user"></i></label>
							<input placeholder="Username" name="username" onChange={this.onChange} value={this.state.username} type="text" required=""/>
						</div>
						
						<div class="form-style-agile">
							<label>Password<i class="fas fa-unlock-alt"></i></label>
							<input placeholder="Password" name="password" onChange={this.onChange} value={this.state.password} type="password" required="" />
						</div>
						<input type="submit" value="Log In"/>
					</form>
				</div>
				<div style={{height:"15vw"}}></div>
			</div>
        );
    }
}
	
function mapStateToProps(state) {	
console.log(state.loginReducer.statuscode)
	return {
	  isLoggedIn: state.loginReducer.is_logged_in,
	  statuscode: state.loginReducer.statuscode
	}
}
  
function mapDispatchToProps(dispatch) {	  
	debugger;
	return {
	  authActions: bindActionCreators(authActions, dispatch),
	};
}
  
export default connect(mapStateToProps, mapDispatchToProps)(Login);