import React, { Component } from 'react';
import '../../assets/css/Footer/footer.css';

class Footer extends Component {

    //initialises variables
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        //displays the footer
        return ( 
            <div className="footer row">
                <div className="col-md-6 text-left ">
                    <div className="info">
                        <h3><span className="inno">Inno</span><span className="minds">minds</span></h3>
                        <p className="copyrights" style={{color:"black"}}>Copyright&copy;2019,<a href="https://www.innominds.com/" style={{color:"blue"}}>Innominds Software Inc.</a>All rights reserved.</p>
                        <p className="email "><a href="" class="pcol" style={{color:"blue"}} >myspace@innominds.com</a></p></div>
                </div>

                <div className="col-md-6 text-right">
                    <img src="https://myspace.innominds.com/public/assets/images/icons/ribbon.jpeg" alt="ribbon"  className="ribbon" />
                    <div className="textonimg">
                        <h6 style={{fontSize:"1.4vw"}}>No.of user Logged in:100</h6>
                    </div>
                </div>

            </div>
         );
    }
}


 
export default Footer;