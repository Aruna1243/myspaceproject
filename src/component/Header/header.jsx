import React, { Component } from 'react';
import Image from './img.png'
import '../../assets/css/Header/header.css'

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchstring: ''
    }
    this.onhandelChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onKeyPressSearch = this.onKeyPressSearch.bind(this)
  }

  onChange = (e) => {
    this.setState({
      searchstring: e.target.value
    });
    e.preventDefault();
  }

  onSubmit = (value) => {
    debugger;
    if (this.state.searchstring != "") {
      window.location.href = "/search/" + value
    }
  }
  onKeyPressSearch(e, value) {
    if (e.key === 'Enter') {
      e.preventDefault();
      if (this.state.searchstring != "") {
        window.location.href = "/search/" + value
      }
    }
  }
  doLogout() {
    if (localStorage.getItem('token') != null) {
      var result = window.confirm("Do You want To logout");
      if (result === true) {
        localStorage.removeItem("token");
        return (window.location.href = '/')
      }
      else {
        return (window.location.href = '/dashboard')
      }
    }
  }
  render() {
    return (
      <div class="header row">
        <div className="col-sm-3">
          <b class="myspace">myspace</b>
        </div>
        <div className="col-sm-9">
          <img class="userImage img" src={Image} alt="logo" />
          <div className="name">
            <h5>Prashanth</h5>
            <p><a class="logout" onClick={this.doLogout} href="" >Logout</a>|<a class="logout" style={{ fontSize: 15 }} href="#">Change password</a></p>
          </div>
          <div class="form-group has-search">
            <span class="faa fa fa-search form-control-feedback"></span>
            <input type="search" class="search" name="searchstring" placeholder="search..by..id" value={this.props.searchstring} onKeyPress={(e) => this.onKeyPressSearch(e, this.state.searchstring)} onChange={this.onhandelChange} />
            <input type="submit" value="search" onClick={() => this.onSubmit(this.state.searchstring)} id="searchbtn" />
          </div>
        </div>
      </div>
    );
  }
}