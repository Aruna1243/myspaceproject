import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from '../Header/header'
import LeftMenu from '../NavBar/leftmenu'
import * as  eventActions from '../../store/actions/eventActions'
import { bindActionCreators } from 'redux';
import '../../assets/css/Events/events.css'
import { Redirect } from 'react-router-dom'
import Footer from '../Footer/footer';

class Events extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: ""
        }
    }

    componentDidMount() {
        debugger;
        this.props.eventActions.fetchEvents();
    }


    handleClick(id, e) {
        debugger;
        if (window.confirm("Do you want to delete article")) {
            this.props.eventActions.deleteEvent(id);
            debugger;
            setTimeout(() => {
                window.location.reload()
            }, 300)
        }
    }

    render() {
        if (localStorage.getItem('token') == null) {
            return (<Redirect to="/" />)
        }
        return (
            <div>
                <Header />
                <div className="row">
                    <div className="col-sm-3">
                        <LeftMenu />
                    </div>
                    <div>
                        {
                            (this.props.EventItems)
                                ? <div class="container-fluid ">
                                    {this.props.EventItems.map(item => (
                                        <div>
                                            <div key={item._id}>
                                                <div class="events" align="center">
                                                    <div class="card notifyCard text-white bg-info  mb-3">
                                                        <div class="card-header">{item.eventName}</div>
                                                        <div class="card-body">
                                                            <p class="card-text">{item.description}</p>
                                                            <p class="card-text">{item.startDate}</p>
                                                            <p class="card-text">{item.endDate}</p>
                                                            <div class="card-footer">
                                                                <button class="btn btn-warning btn-lg" onClick={this.handleClick.bind(this, item.eventId)}>Delete</button>
                                                            </div>
                                                        </div><br /><br />
                                                    </div>
                                                </div>
                                            </div><br />
                                        </div>
                                    ))}
                                </div>
                                : <div>
                                    Loading
                          </div>
                        }
                    </div>
                </div>
                <div className="col-sm-12 " style={{ marginTop: "0px" }}>
                    <Footer />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state.eventReducer.events);
    return {
        EventItems: state.eventReducer.events
    };
}

function mapDispatchToprops(dispatch) {
    return {
        eventActions: bindActionCreators(eventActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToprops)(Events);