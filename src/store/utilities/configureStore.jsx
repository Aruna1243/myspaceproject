import { createStore,applyMiddleware,compose }  from  'redux';
import rootReducer                              from '../reducer/rootReducer';
import authMiddleware                           from '../middleware/authService'
import eventMiddleware                          from '../middleware/eventservice'
import trainingService                          from '../middleware/trainingService';
import salaryService                            from '../middleware/salaryService'
import leaveService                             from '../middleware/leaveServices';
import SearchByIdService                        from '../middleware/searchService';

export default function configureStore(){
    return createStore(
        rootReducer,
        compose(applyMiddleware(
            authMiddleware,
            eventMiddleware,
            trainingService,
            salaryService,
            leaveService,
            SearchByIdService
        ))
    );
};