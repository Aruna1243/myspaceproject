import      request         from 'superagent';
import * as allActions      from '../actions/actionConstants';
import * as eventActions    from '../actions/eventActions';
import * as urlConstants    from '../utilities/constanturls'

const eventService = (store) => next => action => {
    
    next(action)
    switch (action.type) {
        case allActions.FETCH_EVENTS:
            console.log("EVENT service");
            request.get(urlConstants.url+"/displayEvents")
                .then(res => {
                    
                    console.log("success");
                    const data = JSON.parse(res.text);
                    console.log(data);
                    next(eventActions.receiveEvents(data));
                })
        case allActions.DELETE_EVENTS:
            debugger;
            console.log("delete service");
            request.delete(urlConstants.url+"/deleteEvents/" + `${action.payload.id}`)

                .catch(err => {
                    console.log("service failure");
                    next({ type: 'EVENT_DELETE_DATA_ERROR', err });
                })

            break;

        default:
            break;
    }
}
export default eventService;