import      request         from 'superagent';
import * as allActions      from '../actions/actionConstants'
import * as salaryActions   from '../actions/salaryActions'
import * as urlConstants    from '../utilities/constanturls'

const salaryService = (store) => next => action => {
    
    next(action)
    switch(action.type) {
        case allActions.GET_SALARY:
            request.get(urlConstants.url+"/salarydetails/" + `${action.payload}`)
                .then(res => {
                    console.log("success",action.payload);
                    const data =  JSON.parse(res.text);
                    console.log(data);
                    next(salaryActions.receieveSalary(data));
                })
                .catch(err => {
                    console.log("Catch");
                    return next({ type: 'LOGIN_USER_DATA_ERROR',err })
                });
            break;
        default:
            break;
    }
}
export default salaryService;