import      request         from 'superagent';
import * as allActions      from '../actions/actionConstants';
import * as trainingActions from '../actions/trainingActions'
import * as urlConstants    from '../utilities/constanturls'

const trainingService = (store) => next => action => {    
    next(action)
    switch (action.type) {
        case allActions.FETCH_TIME:
            console.log("EVENT service");
            request.get(urlConstants.url+"/trainingSession")
                .then(res => {
                    
                    console.log("success");
                    const data = JSON.parse(res.text);
                    console.log(data);
                    next(trainingActions.receiveTime(data));
                })
                .catch(err => {
                    console.log("service failure");
                    next({ type: 'FETCH_EVENTS_DATA_ERROR', err });
                })

            break;
        default:
            break;

    }
}
export default trainingService;