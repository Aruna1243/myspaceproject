import * as allActions from '../actions/actionConstants';

const initialise = {
    events: [],
    isLoaded: false
}

export default function eventReducer(state = initialise, action) {
    switch (action.type) {
        case allActions.FETCH_EVENTS:
            return action;

        case allActions.RECEIVE_EVENTS: 
            return {
                ...state,
                events: action.payload,
                isLoaded: true
            }
        case allActions.DELETE_EVENTS:
            return action;
    
        default:
            return state
    }
}