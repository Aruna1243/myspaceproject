import { combineReducers }  from 'redux'
import loginReducer         from './authReducer'
import eventReducer         from './eventReducer'
import trainingReducer      from './trainingReducer'
import salaryReducer        from './salaryReducer'
import leaveReducer         from './leaveReducer'
import searchreducer        from './searchReducer'

const rootReducer = combineReducers({
    loginReducer,
    eventReducer,
    trainingReducer,
    salaryReducer,
    leaveReducer,
    searchreducer
});

export default rootReducer;