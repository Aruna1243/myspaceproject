import * as allActions from './actionConstants'

/**
 * Actions for Login
 *  */
export function doLoginUser(data){
    debugger;
    return {type : allActions.DO_LOGIN_USER,payload:data}
}

export function loginUserSucess(data){
    debugger;
    return {type : allActions.LOGIN_USER_SUCCESS,payload:data}
}
/**
 * Actions for fetching MyProfile
 */
export function fetchMyProfile(id){
    return { type : allActions.MY_PROFILE,payload:id }
}

export function receieveMyProfile(data){
    return { type : allActions.MY_PROFILE_RECEIEVE,payload:data }
}

/**
 * Actions for editProfile
 */
export function editProfile(id,data){
    return{ type : allActions.EDIT_PROFILE,payload:{id,data} }
}

export function editProfileSuCcess(data){
    return { type : allActions.EDIT_PROFILE_SUCCESS,payload:data }
}

export function logoutUser(){
    return{type : allActions.LOGOUT_USER,payload:{}};
}