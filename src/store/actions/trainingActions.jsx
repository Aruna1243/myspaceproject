import * as allActions from './actionConstants'

export  function receiveTime(data) {
    return {
        type: allActions.RECEIVE_TIME, payload: data
    }
}

export  function fetchTime() {
    return {
        type: allActions.FETCH_TIME, payload: {}
    }
}
