// npm dependencies
import React, { Component }       from "react";
import { Route, BrowserRouter }   from 'react-router-dom'
import Login                      from './component/Login/login'
import Dashboard                  from './component/Dashboard/dashboard'
import MyProfile                  from './component/MyProfile/myProfile'
import EditProfile                from './component/EditProfile/editProfile'
import CulturalEvents             from './component/CuluralEvents/culturalEvents'
import TrainingCalender           from './component/Training/training'
import SalartDetails              from './component/SalaryDetails/salaryDetails'
import Leave                      from "./component/Leaves/leave";
import Holiday                    from './component/Holidays/holidays'
import Search                     from './component/Search/search'

export default class Router extends React.Component{

    constructor(props) {
        super(props);
      }

      render() {
        
        return (
          <BrowserRouter>
            <div>
              <Route path="/" exact component={Login} />
              <Route path="/dashboard" exact component={Dashboard}/>
              <Route path="/culturalEvents" exact component={CulturalEvents}/>
              <Route path="/myProfile/:id" component={MyProfile}/>
              <Route path="/editProfile/:id" component={EditProfile}/>
              <Route path="/training" component={TrainingCalender}/>
              <Route path="/salaryDetails" component={SalartDetails}/>
              <Route path="/applyLeave" component={Leave}/>
              <Route path="/holiday" component={Holiday}/>
              <Route path="/search/:value" component={Search}/>
            </div>
          </BrowserRouter>
        );
      }
}
    